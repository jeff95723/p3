/*
 * keyboard_interrupt_handler.c
 *
 *  Created on: Nov 11, 2015
 *      Author: jianfeil
 */

#include "../inc/handler/keyboard_interrupt_handler.h"

#include <x86/interrupt_defines.h>
#include <x86/keyhelp.h>
#include <asm.h>

#include "../inc/task/task_mgr.h"
#include "../inc/scheduler/scheduler.h"
#include "../inc/scheduler/context_switch.h"
#include "../inc/util/logger.h"
#include "../inc/util/asm_utils.h"
#include "../inc/util/input_stream.h"
#include "../inc/device_driver/console_driver.h"

void keyboard_handler_init() {
        // Create keystroke queue
        input_stream_init();
}

/**
 * @brief Keyboard handler function that gets called when the keyboard
 * sends a interrupt. Saves the the data from keyboard PIC port to a
 * queue for further processing.
 */
void keyboard_interrupt_handler() {
        char port_data = (inb(KEYBOARD_PORT));
        kh_type k = process_scancode(port_data);
        if (KH_HASDATA(k) && KH_ISMAKE(k) && !KH_ISEXTENDED(k)) {
                //  Get the char and add it to the input stream
                char c = KH_GETCHAR(k);
                int input_empty = input_stream_empty();
                // Test lock before adding?
                input_stream_add(c);

                // If readline calls present echo char
                if (has_pending_readline()) {
                        if ((c == '\b')) {
                                if (!input_empty) {
                                        putbyte(c);
                                }
                        } else {
                                putbyte(c);
                        }
                }

                // If c is new line signal readline cond var
                if (c == '\n') {
                        kcond_signal(&input_stream.cond);
                }
        }
        outb(INT_CTL_PORT, INT_ACK_CURRENT);
        return;
}
