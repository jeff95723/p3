/**
 * @file timer_interrupt_handler.c
 * @brief Implementation for the timer interrupt handler
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#include "../inc/handler/timer_interrupt_handler.h"

#include <x86/interrupt_defines.h>
#include <timer_defines.h>
#include <asm.h>
#include <stddef.h>

#include "../inc/device_driver/timer_device_driver.h"
#include "../inc/task/task_mgr.h"
#include "../inc/scheduler/scheduler.h"
#include "../inc/scheduler/context_switch.h"
#include "../inc/util/logger.h"
#include "../inc/util/asm_utils.h"
#include "../inc/util/variable_queue.h"


void timer_handler_init() {
        outb(TIMER_MODE_IO_PORT, TIMER_SQUARE_WAVE);
        int rate = TIMER_RATE;
        outb(rate & 0xF, TIMER_PERIOD_IO_PORT);
        outb((rate >> 8) & 0xF, TIMER_PERIOD_IO_PORT);
}

/**
 * @brief Implementation for the timer interrupt handler
 *      The timer handler will increment a inner clock tick value
 *      The timer is also responsible for context switching among threads
 */
void timer_interrupt_handler() {
        kern_timer_tick();
        outb(INT_CTL_PORT, INT_ACK_CURRENT);
        if (!(task_manager.switching)) {
                // Disable context switch
                context_switch_lock();
                // get the next thread to run from scheduler
                thread_t * want_to_switch = next_thread();
                if (want_to_switch) {
                        if ((GET_CURRENT_THREAD() != want_to_switch)) {
                                context_switch(want_to_switch);
                        }
                }
                context_switch_unlock();
        }
        return;
}
