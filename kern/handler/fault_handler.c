/**
 * @file fault_handler.c
 * @brief Implementation for the fault handlers
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#include "../inc/handler/fault_handler.h"

#include <cr.h>

#include "../inc/util/logger.h"

void page_fault_handler() {
        Log.e("Page fault. Who cares. %x", get_cr2());
        return;
}

void gp_fault_handler() {
        Log.e("Protection fault. Who cares.");
        return;
}
