/**
 * @file idt_init.c
 * @brief Implementation of IDT installations for fault & syscall handlers
 * @author Jianfei Liu (jianfeil)
 *         Hongyu Li (hongyul)
 * @bug No known bug
 */

#include "../inc/handler/idt_init.h"

#include <idt.h>
#include <seg.h>
#include <idt.h>
#include <stddef.h>
#include <stdint.h>
#include <asm.h>
#include <timer_defines.h>
#include <keyhelp.h>
#include <simics.h>
#include <syscall_int.h>

#include "../inc/handler/asm_device_interrupt_handler_wrapper.h"
#include "../inc/handler/asm_fault_handler_wrapper.h"
#include "../inc/handler/asm_syscall_handler_wrapper.h"


#define HI_16B_MASK (0xFFFF0000)
#define LO_16B_MASK (0x0000FFFF)
#define IDT_CONST_BITS (0x8F)

#define IDT_ENTRY_PRESENT (0x80)
#define DPL_KERN      (0x00)
#define DPL_USER      (0x60)


#define TRAP_GATE_BASE_FLAGS (0xF)
#define INTR_GATE_BASE_FLAGS (0xE)

typedef struct
{
        uint16_t base_lo;             // The lower 16 bits of the jump address.
        uint16_t sel;                 // Kernel segment selector.
        uint8_t  always0;             // This must always be zero.
        uint8_t  flags;               // More flags. See documentation.
        uint16_t base_hi;             // The upper 16 bits of the jump address.
}__attribute__((packed)) idt_entry_t;

int installIDTEntry(int idt_index, unsigned int handler_address,
                uint8_t flags);
int idt_install_usr_trap(int idt_index, unsigned int handler_address);
int idt_install_usr_intr(int idt_index, unsigned int handler_address);
int idt_install_sys_trap(int idt_index, unsigned int handler_address);
int idt_install_sys_intr(int idt_index, unsigned int handler_address);

/**
 * @brief Install all system calls, device interrupt handler and fault handlers
 *        on IDT
 * @return 0 on success
 *         -1 on error
 */
int init_idt_entries() {
//        idt_install_sys_trap(IDT_GP,
//                        (unsigned int) asm_gp_fault_handler_wrapper);
        idt_install_sys_trap(IDT_PF,
                        (unsigned int) asm_page_fault_handler_wrapper);
        idt_install_sys_trap(TIMER_IDT_ENTRY,
                        (unsigned int) asm_timer_interrupt_handler_wrapper);
        idt_install_sys_intr(KEY_IDT_ENTRY,
                        (unsigned int) asm_keyboard_handler_wrapper);
        idt_install_usr_trap(GETTID_INT,
                        (unsigned int) asm_gettid_syscall_wrapper);
        idt_install_usr_trap(FORK_INT,
                        (unsigned int) asm_fork_syscall_wrapper);
        idt_install_usr_trap(EXEC_INT,
                        (unsigned int) asm_exec_syscall_wrapper);
        idt_install_usr_trap(HALT_INT,
                        (unsigned int) asm_halt_syscall_wrapper);
        idt_install_usr_trap(THREAD_FORK_INT,
                        (unsigned int) asm_thread_fork_syscall_wrapper);
        idt_install_usr_trap(SET_STATUS_INT,
                        (unsigned int) asm_set_status_syscall_wrapper);
        idt_install_usr_trap(VANISH_INT,
                        (unsigned int) asm_vanish_syscall_wrapper);
        idt_install_usr_trap(WAIT_INT,
                        (unsigned int) asm_wait_syscall_wrapper);
        idt_install_usr_trap(READLINE_INT,
                        (unsigned int) asm_readline_syscall_wrapper);
        idt_install_usr_trap(PRINT_INT,
                        (unsigned int) asm_print_syscall_wrapper);
        idt_install_usr_trap(GET_CURSOR_POS_INT,
                        (unsigned int) asm_get_cursor_pos_syscall_wrapper);
        idt_install_usr_trap(SET_CURSOR_POS_INT,
                        (unsigned int) asm_set_cursor_pos_syscall_wrapper);
        idt_install_usr_trap(SET_TERM_COLOR_INT,
                        (unsigned int) asm_set_term_color_syscall_wrapper);
        idt_install_usr_trap(NEW_PAGES_INT,
                        (unsigned int) asm_new_pages_syscall_wrapper);
        idt_install_usr_trap(REMOVE_PAGES_INT,
                        (unsigned int) asm_remove_pages_syscall_wrapper);
        return 0;
}

/**
 * @brief A helper function to install a user level trap gate to IDT
 * @param idt_index The index of the entry in IDT to be installed
 * @param handler_address The address of the handler function
 * @return 0 on success
 *         -1 on error
 */
int idt_install_usr_trap(int idt_index, unsigned int handler_address) {
        uint8_t flags = TRAP_GATE_BASE_FLAGS | IDT_ENTRY_PRESENT | DPL_USER;
        return installIDTEntry(idt_index, handler_address, flags);

}

/**
 * @brief A helper function to install a user level interrupt gate to IDT
 * @param idt_index The index of the entry in IDT to be installed
 * @param handler_address The address of the handler function
 * @return 0 on success
 *         -1 on error
 */
int idt_install_usr_intr(int idt_index, unsigned int handler_address) {
        uint8_t flags = INTR_GATE_BASE_FLAGS | IDT_ENTRY_PRESENT | DPL_USER;
        return installIDTEntry(idt_index, handler_address, flags);

}

/**
 * @brief A helper function to install a kernel level trap gate to IDT
 * @param idt_index The index of the entry in IDT to be installed
 * @param handler_address The address of the handler function
 * @return 0 on success
 *         -1 on error
 */
int idt_install_sys_trap(int idt_index, unsigned int handler_address) {
        uint8_t flags = TRAP_GATE_BASE_FLAGS | IDT_ENTRY_PRESENT | DPL_KERN;
        return installIDTEntry(idt_index, handler_address, flags);

}

/**
 * @brief A helper function to install a kernel level interrupt gate to IDT
 * @param idt_index The index of the entry in IDT to be installed
 * @param handler_address The address of the handler function
 * @return 0 on success
 *         -1 on error
 */
int idt_install_sys_intr(int idt_index, unsigned int handler_address) {
        uint8_t flags = INTR_GATE_BASE_FLAGS | IDT_ENTRY_PRESENT | DPL_KERN;
        return installIDTEntry(idt_index, handler_address, flags);

}

/**
 * @brief Installs an idt entry in the idt table with the index idt_index
 * and the handler address handler_address.
 * @param idt_index The index of the idx entry to be installed.
 * @param handler_address The handler address of the idt entry to be
 * installed.
 * @param flags The flags of the entry
 * @return 0 if the install of the idt entry was successful, -1 if not.
 */
int installIDTEntry(int idt_index, unsigned int handler_address,
                uint8_t flags) {
        if (handler_address == 0) {
                // Null Address
                return -1;
        }

        idt_entry_t *base = idt_base();
        base[idt_index].base_lo = handler_address & LO_16B_MASK;
        base[idt_index].sel     = SEGSEL_KERNEL_CS;
        base[idt_index].always0 = 0;
        base[idt_index].flags   = flags;
        base[idt_index].base_hi = (handler_address >> 16) & LO_16B_MASK;
        return 0;
}
