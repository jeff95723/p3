/**
 * @file kmutex.c
 * @brief Implementation for the kernel mutex
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#include "../inc/util/kmutex.h"

#include <stddef.h>

#include "../inc/scheduler/scheduler.h"
#include "../inc/scheduler/context_switch.h"
#include "../inc/util/asm_utils.h"
#include "../inc/task/task_mgr.h"
#include "../inc/util/logger.h"


/**
 * @brief Initialize the given mutex object
 * @param mutex The mutex pointer to be initialized
 */
void kmutex_init(kmutex_t *mutex) {
        mutex->owner_id = -1;
        mutex->active = 1;
        mutex->owner = NULL;
        _Q_INIT_HEAD(&mutex->waiting_queue);
}

/**
 * @brief Deactivate the given mutex object
 *
 *  This function will free the the waiting queue to indicate the mutex has
 *  already been deactivated. All other indicator values will be set to initial
 *  values.
 *
 * @param mutex The mutex pointer to be deactivated
 */
void kmutex_destroy(kmutex_t *mutex) {
        if (Q_EMPTY(&mutex->waiting_queue)) {
                mutex->active = 0;
        }
        return;
}

/**
 * @brief A call to this function ensures mutual exclusion in the region between
 *        itself and a call to kmutex_unlock().
 *
 *  A thread calling this function while another thread is in an interfering
 *  critical section must not proceed until it is able to claim the lock.
 *
 * @param mutex The mutex pointer to be used for processing mutual exclusion
 */
void kmutex_lock(kmutex_t *mutex) {
        if (mutex == NULL || !mutex->active) return;

        // disable context switch to protect queue operation
        context_switch_lock();
        thread_t *thread = GET_CURRENT_THREAD();
        if (mutex->owner_id == -1) {
                mutex->owner_id = thread->tid;
                mutex->owner = (void *) thread;
                context_switch_unlock();
                return;
        }

        // if not the owner of the lock
        if (thread->tid != mutex->owner_id) {
                // add to waiting queue
                _Q_INSERT_TAIL(&(mutex->waiting_queue), thread, mutex_wait);
                thread_t *want_to_switch = (thread_t *) mutex->owner;
                if (want_to_switch->state == THREAD_RUNNING) {
                        _Q_REMOVE(&task_manager.runqueue,
                                        want_to_switch, runnable);
                        _Q_INSERT_FRONT(&task_manager.runqueue,
                                        want_to_switch, runnable);
                } else {
                        want_to_switch = next_thread();
                }
                // remove from run queue, and switch to new thread
                remove_runnable_thread(thread);
                context_switch(want_to_switch);
        }
        context_switch_unlock();
        return;
}

/**
 * @brief Signals the end of a region of mutual exclusion
 *
 *  The calling thread gives up its claim to the lock, and notify the next
 *  thread on waiting queue to start executing.
 *
 * @param mutex The mutex pointer to be used for processing mutual exclusion
 */
void kmutex_unlock(kmutex_t *mutex) {
        if (mutex == NULL || !mutex->active) return;

        // disable context switch to protect queue operation
        context_switch_lock();

        // get the first thread on the waiting queue
        thread_t *next = _Q_GET_FRONT(&(mutex->waiting_queue));
        if (next) {
                // remove it from waiting queue & add to run queue
                _Q_REMOVE(&(mutex->waiting_queue), next, mutex_wait);
                insert_runnable_thread(next);
                mutex->owner_id = next->tid;
                mutex->owner = (void *) next;
        } else {
                mutex->owner_id = -1;
                mutex->owner = NULL;
        }
        context_switch_unlock();
        return;
}

/**
 * @brief Check whether the mutex lock has been acquired
 * @param mutex The mutex pointer to be checked possession
 * @return 1 if the mutex is locked
 *         0 if the mutex is free
 */
int kmutex_is_locked(kmutex_t *mutex) {
        return (mutex->owner_id != -1);
}
