/*
 * input_stream.c
 *
 *  Created on: Nov 11, 2015
 *      Author: jianfeil
 */

#include <stddef.h>
#include <malloc.h>
#include "../inc/util/input_stream.h"
#include "../inc/util/logger.h"

stream_t input_stream;

stream_data_t* create_data_chunk();
void input_stream_backspace();

stream_data_t* create_data_chunk() {
        Log.d("Creating new data chunk");
        stream_data_t *chunk = calloc(1, sizeof(stream_data_t));
        if (chunk == NULL) {
                // Malloc failed
                Log.d("Input stream data chunk malloc failed.");
                return NULL;
        }
        chunk->read_index = 0;
        chunk->write_index = 0;
        Log.d("Created new data chunk");
        return chunk;
}

int is_chunk_full_or_null(stream_data_t *chunk) {
        if (!chunk) return 1;
        return (chunk->write_index ==  DATA_BUFLEN);
}

int is_chunk_empty(stream_data_t *chunk) {
        return (chunk->write_index ==  0) ||
                        (chunk->write_index == chunk->read_index);
}

void input_stream_init() {
        input_stream.lines = 0;
        kmutex_init(&input_stream.mutex);
        kcond_init(&input_stream.cond);
        Q_INIT_HEAD(&input_stream.data);
}

int input_stream_empty() {
        stream_data_t *tail = Q_GET_TAIL(&input_stream.data);
        if (!tail) return 1;
        return is_chunk_empty(tail);
}

void input_stream_add(char c) {
        stream_data_t *tail = Q_GET_TAIL(&input_stream.data);

        if (c == '\b') {
                input_stream_backspace();
                tail = Q_GET_TAIL(&input_stream.data);
                Log.d("Read Index: %d, Write index: %d", tail->read_index, tail->write_index);
                return;
        }

        if (is_chunk_full_or_null(tail)) {
                // Create a new data chunk and add to tail
                stream_data_t *chunk = create_data_chunk();
                if (!chunk) return;
                Q_INIT_ELEM(chunk, stream_data);
                Q_INSERT_TAIL(&input_stream.data, chunk, stream_data);
                tail = Q_GET_TAIL(&input_stream.data);
        }

        tail->buf[tail->write_index] = c;
        tail->write_index++;
        Log.d("Read Index: %d, Write index: %d", tail->read_index, tail->write_index);
        if (c == '\n') {
//                kmutex_lock(&input_stream.mutex);
                input_stream.lines++;
//                kmutex_unlock(&input_stream.mutex);
        }
}

void input_stream_backspace() {
        stream_data_t *tail = Q_GET_TAIL(&input_stream.data);
        // Nothing to delete
        if (tail == NULL) return;
        if (is_chunk_empty(tail)) {
                Log.d("Chunk empty, delete chunk");
                Q_REMOVE_TAIL(&input_stream.data, tail, stream_data);
                tail = Q_GET_TAIL(&input_stream.data);
                Log.d("Deleted chunk");
        }

        // Not empty, just decrement write index
        tail->write_index--;
        return;
}

int input_stream_consume() {
        stream_data_t* front;
        if ((front = Q_GET_FRONT(&input_stream.data)) == NULL) {
                return -1;
        }

        // No data was written to that point yet
        if (front->read_index >= front->write_index) return -1;
        char res = front->buf[front->read_index];
        if (res == '\n') {
//                kmutex_lock(&input_stream.mutex);
                input_stream.lines--;
//                kmutex_unlock(&input_stream.mutex);
        }
        front->read_index++;
        // Whole chunk was read, free that chunk
        if (front->read_index == DATA_BUFLEN) {
                Q_REMOVE_FRONT(&input_stream.data, front,stream_data);
                free(front);
        }
        return res;
}

int has_pending_readline() {
        return (input_stream.cond.waiting_queue.size != 0);
}
