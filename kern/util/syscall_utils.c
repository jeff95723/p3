/**
 * @file syscall_utils.c
 * @brief Implementation for the system call utilities
 * @author Jianfei Liu (jianfeil)
 * @bug No known bug
 */

#include "../inc/util/syscall_utils.h"

#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <syscall.h>
#include <cr.h>
#include <exec2obj.h>
#include <common_kern.h>

#include "../inc/mem/vm.h"


int mem_valid(void *p) {
        void *physical;
        void *dir_table = (void *)get_cr3();

        if (virtual_to_physical_address(dir_table, p, &physical) < 0) {
                return -1;
        } else {
                return 0;
        }

        // TODO: check writable?
}

/**
 * Checks whether the argument packet memory address is valid with sufficient
 * permissions.
 * @param p the pointer to the packet
 * @param size size of the packet
 * @return 1 if the address is valid and has user access, 0 if the address
 * if valid and has kernel address, -1 if the address is not valid.
 */
int arg_packet_mem_valid(void *p, int size) {
        // TODO: Check multiple pages when needed.
        void *physical;
        void *dir_table = (void *)get_cr3();

        while (size > 0) {
                if (virtual_to_physical_address(dir_table, p, &physical) < 0) {
                        return -1;
                }
                if (GET_READ_WRITE(physical) == 0) return -1;

                if (!GET_USER_ACCESS(physical)) {
                        return 0;
                }
                size -= PAGE_SIZE;
                p = (char *) p + PAGE_SIZE;
        }
        return 1;
}

/**
 * Checks whether the memory address is available for allocation
 * @param p the pointer to the start of the memory to be checked
 * @param size size of the memory to be checked
 * @return 1 if the address is available, 0 if the memory is not available
 * if valid and has kernel address, -1 if the address is not valid.
 */
int mem_available(void *p, int size) {
        // TODO: Check multiple pages when needed.
        void *physical;
        void *dir_table = (void *)get_cr3();

        while (size > 0) {
                if ((virtual_to_physical_address(dir_table, p, &physical)
                                != -1) || (uint32_t) p < USER_MEM_START) {
                        return 0;
                }
                size -= PAGE_SIZE;
                p = (char *) p + PAGE_SIZE;
        }
        return 1;
}


int prog_name_valid(char *name) {
        char buf[MAX_EXECNAME_LEN + 1];
        memset(buf, 0, MAX_EXECNAME_LEN + 1);
        strncpy(buf, name, MAX_EXECNAME_LEN);
        buf[MAX_EXECNAME_LEN] = '\0';

        int i;

        for (i = 0; i < exec2obj_userapp_count; i++) {
                if (strcmp((&exec2obj_userapp_TOC[i])->execname, buf) == 0) {
                        return 1;
                }
        }

        // Name not found
        return 0;
}
