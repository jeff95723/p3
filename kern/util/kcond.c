/**
 * @file kcond.c
 * @brief Implementation for the kernel conditional variable
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#include "../inc/util/kcond.h"

#include <stddef.h>

#include "../inc/task/task_mgr.h"
#include "../inc/scheduler/context_switch.h"
#include "../inc/scheduler/scheduler.h"
#include "../inc/util/logger.h"

/**
 * @brief Initialize the condition variable pointed to by cv
 * @param cv The condition variable pointer to be initialized
 * @return 0 on success
 *         -1 on error
 */
void kcond_init(kcond_t *cv) {
        cv->active = 1;
        kmutex_init(&cv->mutex);
        _Q_INIT_HEAD(&cv->waiting_queue);
}

/**
 * @brief Deactivate the condition variable pointed to by cv
 * @param cv The condition variable pointer to be deactivated
 */
void kcond_destroy(kcond_t *cv) {
        if (Q_EMPTY(&cv->waiting_queue)) {
                cv->active = 0;
                kmutex_destroy(&cv->mutex);
        }
}

/**
 * @brief Allows a thread to wait for a condition and release the associated
 *        mutex that it needs to hold to check that condition
 * @param cv The pointer to the conditional variable for threads to wait
 * @param mp The pointer to the mutex lock
 */
void kcond_wait(kcond_t *cv, kmutex_t *mp) {
        if (cv == NULL || mp == NULL || !cv->active) {
                return;
        }
        kmutex_lock(&cv->mutex);
        // insert to waiting queue
        thread_t *thread = GET_CURRENT_THREAD();
        _Q_INSERT_TAIL(&cv->waiting_queue, thread, cond_wait);
        kmutex_unlock(mp);
        kmutex_unlock(&cv->mutex);

        // atomic deschedule
        context_switch_lock();
        Log.d("kcond_wait(): prepare to deschedule myself");
        thread_t *want_to_switch = next_thread();
        remove_runnable_thread(thread);
        context_switch(want_to_switch);
        context_switch_unlock();

        kmutex_lock(mp);
        return;
}

/**
 * @brief Wake up a thread waiting on the condition variable pointed to by cv
 * @param cv The pointer to the conditional variable for threads to wait
 */
void kcond_signal(kcond_t *cv) {
        Log.d("kcond_signal(): enter cond signal");
        if (cv == NULL || !cv->active) {
                Log.e("kcond_signal(): cv is null???");
                return;
        }
        kmutex_lock(&cv->mutex);
        // if waiting queue empty, just return
        if (Q_EMPTY(&cv->waiting_queue)) {
                Log.d("kcond_signal(): waiting is empty.");
                kmutex_unlock(&cv->mutex);
        } else {
                Log.d("kcond_signal(): prepare to wake up next thread");
                // get the first thread on the waiting queue
                context_switch_lock();
                thread_t *next = _Q_GET_FRONT(&(cv->waiting_queue));
                if (next) {
                        // remove it from waiting queue & add to run queue
                        _Q_REMOVE(&(cv->waiting_queue), next, cond_wait);
                        insert_runnable_thread(next);
                }
                context_switch_unlock();
                kmutex_unlock(&cv->mutex);
        }
        return;
}

/**
 * @brief Wake up all threads waiting on the condition variable pointed to by cv
 * @param cv The pointer to the conditional variable for threads to wait
 */
void kcond_broadcast(kcond_t *cv) {
        if (cv == NULL || !cv->active) {
                return;
        }
        kmutex_lock(&cv->mutex);
        while (!Q_EMPTY(&cv->waiting_queue)) {
                thread_t *next = _Q_GET_FRONT(&(cv->waiting_queue));
                _Q_REMOVE(&(cv->waiting_queue), next, cond_wait);
                insert_runnable_thread(next);
        }
        kmutex_unlock(&cv->mutex);
        return;
}
