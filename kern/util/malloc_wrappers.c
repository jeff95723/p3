#include <stddef.h>
#include <malloc.h>
#include <malloc_internal.h>

#include "../inc/scheduler/context_switch.h"

/* safe versions of malloc functions */
void *malloc(size_t size) {
        context_switch_lock();
        void *ptr = _malloc(size);
        context_switch_unlock();
        return ptr;
}

void *memalign(size_t alignment, size_t size) {
        context_switch_lock();
        void *ptr = _memalign(alignment, size);
        context_switch_unlock();
        return ptr;
}

void *calloc(size_t nelt, size_t eltsize) {
        context_switch_lock();
        void *ptr = _calloc(nelt, eltsize);
        context_switch_unlock();
        return ptr;
}

void *realloc(void *buf, size_t new_size) {
        context_switch_lock();
        void *ptr = _realloc(buf, new_size);
        context_switch_unlock();
        return ptr;
}

void free(void *buf) {
        context_switch_lock();
        _free(buf);
        context_switch_unlock();
        return;
}

void *smalloc(size_t size) {
        context_switch_lock();
        void *ptr = _smalloc(size);
        context_switch_unlock();
        return ptr;
}

void *smemalign(size_t alignment, size_t size) {
        context_switch_lock();
        void *ptr = _smemalign(alignment, size);
        context_switch_unlock();
        return ptr;
}

void sfree(void *buf, size_t size) {
        context_switch_lock();
        _sfree(buf, size);
        context_switch_unlock();
        return;
}

