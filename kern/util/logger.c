/**
 * @file logger.c
 * @brief The implementation for the logger tool
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#include "../inc/util/logger.h"

#include <simics.h>
#include <stdarg.h>
#include <stdio/stdio.h>
#include <string.h>


logger_t Log;

/**
 * @brief A helper method to print the given msg with prefix and given arguments
 * @param prefix The prefix of the message
 * @param msg The format of the message to be printed
 * @param ap The arguments for the message format
 */
void logger_print(char *prefix, char *msg, va_list ap) {
        char msg_buf[MSG_BUF_LENGTH];
        snprintf (msg_buf, MSG_BUF_LENGTH - 1, "%s --- %s", prefix, msg);

        char str[MSG_BUF_LENGTH];
        vsnprintf (str, MSG_BUF_LENGTH - 1, msg_buf, ap);

        sim_puts(str);
}

/**
 * @brief Initialize the logger with given message level
 * @param level The message level of the logger
 */
void logger_init(int level) {
        Log.level = level;
        Log.d = d;
        Log.e = e;
        Log.i = i;
        Log.v = v;
        Log.w = w;
}

/**
 * @brief Print a debug message
 * @param msg The format of the message
 */
void d(char *msg, ...) {
        if (DEBUG_LEVEL > Log.level) return;
        va_list ap;
        va_start(ap, msg);
        logger_print("DEBUG", msg, ap);
        va_end(ap);
}

/**
 * @brief Print a error message
 * @param msg The format of the message
 */
void e(char *msg, ...) {
        if (ERROR_LEVEL > Log.level) return;
        va_list ap;
        va_start(ap, msg);
        logger_print("ERROR", msg, ap);
        va_end(ap);
}

/**
 * @brief Print a info message
 * @param msg The format of the message
 */
void i(char *msg, ...) {
        if (INFO_LEVEL > Log.level) return;
        va_list ap;
        va_start(ap, msg);
        logger_print("INFO", msg, ap);
        va_end(ap);
}

/**
 * @brief Print a verbose message
 * @param msg The format of the message
 */
void v(char *msg, ...) {
        if (VERBOSE_LEVEL > Log.level) return;
        va_list ap;
        va_start(ap, msg);
        logger_print("VERBOSE", msg, ap);
        va_end(ap);
}

/**
 * @brief Print a warning message
 * @param msg The format of the message
 */
void w(char *msg, ...) {
        if (WARNING_LEVEL > Log.level) return;
        va_list ap;
        va_start(ap, msg);
        logger_print("WARNING", msg, ap);
        va_end(ap);
}
