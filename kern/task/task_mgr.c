/**
 * @file task_mgr.c
 * @brief The implementation for the task manager module
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

//TODO: change _malloc, _free and _smemalign

#include "../inc/task/task_mgr.h"

#include <stddef.h>
#include <syscall.h>
#include <malloc.h>
#include <simics.h>

#include "../inc/scheduler/scheduler.h"
#include "../inc/scheduler/context_switch.h"
#include "../inc/util/variable_queue.h"
#include "../inc/util/asm_utils.h"
#include "../inc/util/logger.h"
#include "../inc/util/kcond.h"
#include "../inc/mem/vm.h"

task_mgr_t task_manager;

int task_elem_key(task_t *e) {
        return e->pid;
}

int thread_elem_key(thread_t *e) {
        return e->tid;
}

int key_cmp(int k1, int k2) {
        return (k1 == k2);
}

unsigned int key_hash(int k) {
        return k * 23 + 17;
}

void task_ht_init(task_hash_table *ht, int capacity) {
        int index;
        task_queue_t **task_chain = calloc(capacity, sizeof(task_queue_t *));
        for (index = 0; index < capacity; index++) {
                task_chain[index] = malloc(sizeof(task_queue_t));
        }
        HT_INIT_HEAD(ht, capacity, task_chain,
                        task_elem_key, key_cmp, key_hash);
        HT_INIT_CHAIN(ht);
}

void thread_ht_init(thread_hash_table *ht, int capacity) {
        int index;
        thread_queue_t **thread_chain =
                        calloc(capacity, sizeof(thread_queue_t));
        for (index = 0; index < capacity; index++) {
                thread_chain[index] = malloc(sizeof(thread_queue_t));
        }
        HT_INIT_HEAD(ht, capacity, thread_chain,
                        thread_elem_key, key_cmp, key_hash);
        HT_INIT_CHAIN(ht);
}

/**
 * @brief Initialize the task manager
 */
void task_init() {
        task_manager.next_tid = 1;              // tid start from 1
        task_manager.switching = 0;             // enable context switch
        task_manager.idle_task = NULL;
        task_manager.init_task = NULL;
        kmutex_init(&task_manager.mutex);       // init task manager mutex
        kmutex_init(&task_manager.print_mutex); // init kernel print mutex
        Q_INIT_HEAD(&task_manager.task_queue);  // init task traversal queue
        Q_INIT_HEAD(&task_manager.runqueue);    // init run queue

        task_ht_init(&task_manager.task_ht, 256);       // init task table
        thread_ht_init(&task_manager.thread_ht, 256);   // init thread table
}

/**
 * @brief Create a new task with a new kernel thread
 * @return The pointer to the new task struct
 */
task_t *create_task() {
        task_t *task = malloc(sizeof(task_t));
        if (task == NULL) {
                Log.e("create_task(): malloc task failed.");
                return NULL;
        }

        // init task mutex lock
        kmutex_init(&task->mutex);
        kcond_init(&task->cv);

        // create new thread
        Q_INIT_HEAD(&task->thread_queue);
        thread_t *new_thread = create_thread(task);
        if (new_thread == NULL) {
                kmutex_destroy(&task->mutex);
                free(task);
                return NULL;
        }
        task->thread = new_thread;

        // init task attributes
        task->pid = new_thread->tid;
        task->state = TASK_RUNNING;

        // init task relation
        Q_INIT_HEAD(&task->children);
        Q_INIT_HEAD(&task->dead_children);
        Q_INIT_HEAD(&task->np_calls);

        // add to task queue
        Q_INIT_ELEM(task, iterator);
        Q_INSERT_TAIL(&task_manager.task_queue, task, iterator);

        // add to hash table
        HT_INIT_ELEM(task, chain);
        HT_INSERT(&task_manager.task_ht, task, chain);

        return task;
}

/**
 * @brief Create new thread and set up the kernel stack for the new thread
 * @param task The task that spawns this new thread
 * @return The pointer of the new thread struct
 */
thread_t *create_thread(task_t *task) {
        // create thread stack union
        thread_stack *stack = smemalign(PAGE_SIZE * 2, sizeof(thread_stack));
        if (stack == NULL) {
                Log.e("create_thread(): smemallign thread stack failed.");
                return NULL;
        }
        thread_t *thread = &(stack->thread);

        // init thread attributes
        thread->state = -1;
        thread->task = task;
        kmutex_lock(&task_manager.mutex);
        thread->tid = task_manager.next_tid;
        task_manager.next_tid++;
        kmutex_unlock(&task_manager.mutex);

        // add to task's thread queue
        Q_INIT_ELEM(thread, iterator);
        Q_INSERT_TAIL(&task->thread_queue, thread, iterator);

        // add to hash table
        HT_INIT_ELEM(thread, chain);
        HT_INSERT(&task_manager.thread_ht, thread, chain);

        return thread;
}

void destroy_task(task_t *task) {
        if (!task) {
                Log.e("Try to destroy a NULL task.");
                return;
        }
        if (task->state != TASK_DEAD) {
                Log.e("Try to destroy a live task.");
                return;
        }
        // remove from parent's children queue
        if (task->parent) {
                Q_REMOVE(&(task->parent->children), task, sibling);
        }
        // remove from task manager's task queue
        Q_REMOVE(&(task_manager.task_queue), task, iterator);

        // destroy mutex and cv
        kmutex_destroy(&task->mutex);
        kcond_destroy(&task->cv);

        // notify all children that their parent died
        task_t *child;
        Q_FOREACH(child, &(task->children), sibling) {
                child->parent = NULL;
        }

        // TODO: remove from hash table probably ?
        task_t *rm_task;
        HT_REMOVE(&(task_manager.task_ht), task->pid, rm_task, chain);

        // TODO: vm free ?
        context_switch_lock();
        free_dir_table(task->dir_table);
        context_switch_unlock();

        // free task struct
        free(task);
}

void destroy_thread(thread_t *thread) {
        if (!thread) {
                Log.e("Thread to be destroyed is NULL.");
                return;
        }
        if (thread->state != THREAD_DEAD) {
                Log.e("Try to destroy a live thread.");
        }

        // remove from task's thread queue
        if (thread->task) {
                Q_REMOVE(&(thread->task->thread_queue), thread, iterator);
        }

        // TODO: remove from hash table probably ?
        thread_t *rm_thread;
        HT_REMOVE(&(task_manager.thread_ht), thread->tid, rm_thread, chain);

        // free thread's kernel stack
        sfree(thread, 2 * PAGE_SIZE);
}
void thread_vanish(thread_t *thread) {
        Log.d("Start thread_vanish %d", thread->tid);
        task_t *task = thread->task;
        task_t *init = task_manager.init_task;

        kmutex_lock(&task->mutex);
        _Q_REMOVE(&task->thread_queue, thread, iterator);
        // if last thread in task, kill task
        Log.d("Task %d queue size = %d", task->pid, Q_SIZE(&task->thread_queue));
        if (Q_EMPTY(&task->thread_queue)) {
                task->state = TASK_DEAD;

                // TODO: transfer exit_queue to parent (or init) ?
                kmutex_lock(&init->mutex);
                Q_CONCAT(&(init->dead_children),
                                &(task->dead_children), dead_sibling);
                //kcond_broadcast(&(init->cv));
                kmutex_unlock(&init->mutex);

                // TODO: add task to parent's dead queue
                // TODO: need to check parent existence
                Q_INIT_ELEM(task, dead_sibling);
                if (task->parent && task->parent->state != TASK_DEAD) {
                        Q_INSERT_FRONT(&(task->parent->dead_children),
                                        task, dead_sibling);
                        kcond_signal(&(task->parent->cv));
                } else {
                        Q_INSERT_FRONT(&(init->dead_children),
                                        task, dead_sibling);
                        kcond_signal(&(init->cv));
                }
        }
        kmutex_unlock(&task->mutex);

        // remove from run queue
        context_switch_lock();
        remove_runnable_thread(thread);
        thread->state = THREAD_DEAD;
        context_switch_unlock();
}
