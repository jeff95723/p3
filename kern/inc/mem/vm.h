/**
 * @file vm.h
 * @brief Interface for the virtual memory module
 * @author Jianfei Liu (jianfeil)
 * @bug No known bug
 */

#ifndef KERN_VM_H_
#define KERN_VM_H_

#include <stdint.h>

//////////////////////////////////////////////////////////////////////
// VM Macros
//////////////////////////////////////////////////////////////////////
#define SET_PRESENT(p)          (p = (void *)((unsigned int) p | (1)))
#define UNSET_PRESENT(p)        (p = (void *)((unsigned int) p & (~1)))
#define SET_READ_WRITE(p)       (p = (void *)((unsigned int) p | (1 << 1)))
#define UNSET_READ_WRITE(p)     (p = (void *)((unsigned int) p & (~(1 << 1))))
#define SET_USER_ACCESS(p)      (p = (void *)((unsigned int) p | (1 << 2)))
#define UNSET_USER_ACCESS(p)    (p = (void *)((unsigned int) p & (~(1 << 2))))
#define SET_WRITE_TRU(p)        (p = (void *)((unsigned int) p | (1 << 3)))
#define UNSET_WRITE_TRU(p)      (p = (void *)((unsigned int) p & (~(1 << 3))))
#define SET_DISABLE_CACHE(p)    (p = (void *)((unsigned int) p | (1 << 4)))
#define UNSET_DISABLE_CACHE(p)  (p = (void *)((unsigned int) p & (~(1 << 4))))
#define SET_ACCESSED(p)         (p = (void *)((unsigned int) p | (1 << 5)))
#define UNSET_ACCESSED(p)       (p = (void *)((unsigned int) p & (~(1 << 5))))
#define SET_DIRTY(p)            (p = (void *)((unsigned int) p | (1 << 6)))
#define UNSET_DIRTY(p)          (p = (void *)((unsigned int) p & (~(1 << 6))))
#define SET_PAGE_SIZE_FLAG(p)   (p = (void *)((unsigned int) p | (1 << 7)))
#define UNSET_PAGE_SIZE_FLAG(p) (p = (void *)((unsigned int) p & (~(1 << 7))))
#define SET_GLOBAL_FLAG(p)      (p = (void *)((unsigned int) p | (1 << 8)))
#define UNSET_GLOBAL_FLAG(p)    (p = (void *)((unsigned int) p & (~(1 << 8)))


#define GET_DIR_INDEX(p) (((unsigned int) p & 0xFFC00000) >> 22)
#define GET_PAGE_INDEX(p) (((unsigned int) p & 0x003FF000) >> 12)
#define FLUSH_VM_FLAGS(p) ((unsigned int) p & 0xFFFFF000)
#define GET_VM_FLAGS(p) ((unsigned int) p & 0x00000FFF)
#define PAGE_TABLE_SIZE (1 << 10)
#define DIR_TABLE_SIZE (1 << 10)

#define PAGE_SUPERVISOR_ACCESS 0
#define PAGE_USER_ACCESS 1
#define PAGE_READ_ONLY 0
#define PAGE_READ_WRITE 1
#define IDENTITY_MAPPING_SIZE (1 << 24)

#define GET_PRESENT(p) ((unsigned int)p & 1)
#define GET_READ_WRITE(p)       ((unsigned int) p & (1 << 1))
#define GET_USER_ACCESS(p)      ((unsigned int) p & (1 << 2))
#define GET_WRITE_TRU(p)        ((unsigned int) p & (1 << 3))
#define GET_DISABLE_CACHE(p)    ((unsigned int) p & (1 << 4))
#define GET_ACCESSED(p)         ((unsigned int) p & (1 << 5))
#define GET_DIRTY(p)            ((unsigned int) p & (1 << 6))
#define GET_PAGE_SIZE_FLAG(p)   ((unsigned int) p & (1 << 7))
#define GET_GLOBAL_FLAG(p)      ((unsigned int) p & (1 << 8))


void init_paging();

void enable_paging();

void disable_paging();

void *get_identity_paging_table();

void *new_directory_table_with_id();

void *new_page_table();

void copy_dir_table(void *src_dir_table, void *dst_dir_table, void *buf_addr);

void free_dir_table(void *dir_table);

void *add_newpage(void *dst_dir_table,
                  void *src_vm_addr,
                  int read_write_flag,
                  int user_supervisor_flag);

void remove_newpage(void *dst_dir_table, void *src_vm_addr);

unsigned int remap_page(void *src_dir_table,
                        void *buf_addr,
                        unsigned int target_entry);

void *init_page_entry_flags(void *page_entry,
                            int read_write_flag,
                            int user_access_flag);

int virtual_to_physical_address(void *dir_table, void *vm, void ** p);

void invalidate_vm_page(void * page);

//void free();

#endif /* KERN_VM_H_ */
