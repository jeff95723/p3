/**
 * @file program_loader.h
 * @brief Interface for the program loader module
 * @author Jianfei Liu (hongyul)
 * @bug No known bug
 */

#ifndef KERN_PROGRAM_LOADER_H_
#define KERN_PROGRAM_LOADER_H_

#include "../task/task_mgr.h"

#define STACK_SIZE (40)
#define STACK_START (0xc0093000)

task_t *load_program(const char *name);

int load_elf(const char *name, void *dir_addr_p, void **stack_bottom_p,
        void **entry_p);

#endif /* KERN_PROGRAM_LOADER_H_ */
