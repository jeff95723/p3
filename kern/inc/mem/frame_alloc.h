/**
 * @file frame_alloc.h
 * @brief The interface for the physical frame allocator
 * @author Jianfei Liu (jianfeil)
 * @bug No known bug
 */

#ifndef KERN_FRAME_ALLOC_H_
#define KERN_FRAME_ALLOC_H_

#include <common_kern.h>

#include "../util/kmutex.h"

// number of physical pages for kernel
#define KERNEL_PAGE_NUM (USER_MEM_START >> 12);

typedef struct {
        unsigned int size;              // current size of pages allocated
        unsigned int capacity;          // max number of pages for allocation
        unsigned int new_frame_addr;    // address of a new frame
        void *free_frame;               // address of the first free frame
        void *buffer;                   // kernel buffer page for remapping
        kmutex_t mutex;                 // mutex lock for frame allocator
} frame_alloc_t;

/* Initializer for the physical frame allocator module */
void frame_alloc_init();

/* Returns the start address of a free frame */
void *new_frame();

/* Recycles the given physical frame */
void free_frame(void *free_phys_addr);

/* Returns the number of free frames */
int available_frames();

#endif /* KERN_FRAME_ALLOC_H_ */
