/**
 * @file run_program.h
 * @brief
 * @author Jianfei Liu (jianfeil)
 * @bug No known bug
 */

#ifndef KERN_RUN_PROGRAM_H_
#define KERN_RUN_PROGRAM_H_

void run_program(void * dir_addr, void *stack_addr, void *entry_addr,
        void *new_eflags);

#endif /* KERN_RUN_PROGRAM_H_ */
