/**
 * @file loader.h
 * @brief Structure definitions, #defines, and function prototypes
 *        for the user process loader.
 * @author Jianfei Liu (jianfeil)
 *         Hongyu Li (hongyul)
 * @bug No known bug
 */

#ifndef _LOADER_H
#define _LOADER_H


/* --- Prototypes --- */

int getbytes( const char *filename, int offset, int size, char *buf );

/*
 * Declare your loader prototypes here.
 */

#endif /* _LOADER_H */
