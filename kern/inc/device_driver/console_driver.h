/*
 * console_driver.h
 *
 *  Created on: Nov 12, 2015
 *      Author: liujianfei
 */

#ifndef KERN_INC_DEVICE_DRIVER_CONSOLE_DRIVER_H_
#define KERN_INC_DEVICE_DRIVER_CONSOLE_DRIVER_H_

void console_driver_init();

int putbyte(char ch);

void putbytes(const char *s, int len);
int console_set_term_color(int color);
int console_set_cursor(int row, int col);
void console_get_cursor(int *row, int *col);

#endif /* KERN_INC_DEVICE_DRIVER_CONSOLE_DRIVER_H_ */
