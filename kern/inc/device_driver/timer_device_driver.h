/**
 * @file timer_device_driver.h
 * @brief The interface for the timer device driver module
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#ifndef KERN_INC_DEVICE_DRIVER_TIMER_DEVICE_DRIVER_H_
#define KERN_INC_DEVICE_DRIVER_TIMER_DEVICE_DRIVER_H_

void kern_timer_init();

void kern_timer_tick();

unsigned int kern_timer_get_ticks();

#endif /* KERN_INC_DEVICE_DRIVER_TIMER_DEVICE_DRIVER_H_ */
