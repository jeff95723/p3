/**
 * @file scheduler.h
 * @brief Interface for the scheduler module
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#ifndef KERN_SCHEDULER_H_
#define KERN_SCHEDULER_H_

#include "../task/task_mgr.h"

thread_t *next_thread();

void insert_runnable_thread(thread_t *thread);

void remove_runnable_thread(thread_t *thread);

#endif /* KERN_SCHEDULER_H_ */
