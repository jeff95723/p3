/**
 * @file context_switch.h
 * @brief The interface for the context switch module
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#ifndef KERN_CONTEXT_SWITCH_H_
#define KERN_CONTEXT_SWITCH_H_

#include "../task/task_mgr.h"

void context_switch_lock();

void context_switch_unlock();

/**
 * @brief Context switch to the thread described by the given thread struct
 * @param thread The thread to switch to
 */
void context_switch(thread_t *thread);

#endif /* KERN_CONTEXT_SWITCH_H_ */
