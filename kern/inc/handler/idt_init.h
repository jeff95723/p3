/**
 * @file idt_init.h
 * @brief Interface for IDT installation module
 * @author Jianfei Liu (jianfeil)
 * @bug No known bug
 */

#ifndef KERN_IDT_INIT_H_
#define KERN_IDT_INIT_H_

int init_idt_entries();

#endif /* KERN_IDT_INIT_H_ */
