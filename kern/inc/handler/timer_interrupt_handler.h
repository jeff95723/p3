/**
 * @file timer_interrupt_handler.h
 * @brief Interface for the timer interrupt handler
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#ifndef KERN_TIMER_INTERRUPT_HANDLER_H_
#define KERN_TIMER_INTERRUPT_HANDLER_H_

void timer_handler_init();

void timer_interrupt_handler();

#endif /* KERN_TIMER_INTERRUPT_HANDLER_H_ */
