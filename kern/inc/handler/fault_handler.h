/**
 * @file fault_handler.h
 * @brief Interface for fault handlers
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#ifndef KERN_FAULT_HANDLER_H_
#define KERN_FAULT_HANDLER_H_

void page_fault_handler();
void gp_fault_handler();

#endif /* KERN_FAULT_HANDLER_H_ */
