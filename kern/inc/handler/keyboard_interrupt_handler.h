/*
 * keyboard_interrupt_handler.h
 *
 *  Created on: Nov 11, 2015
 *      Author: jianfeil
 */

#ifndef KERN_INC_HANDLER_KEYBOARD_INTERRUPT_HANDLER_H_
#define KERN_INC_HANDLER_KEYBOARD_INTERRUPT_HANDLER_H_

void keyboard_handler_init();

void keyboard_interrupt_handler();

#endif /* KERN_INC_HANDLER_KEYBOARD_INTERRUPT_HANDLER_H_ */
