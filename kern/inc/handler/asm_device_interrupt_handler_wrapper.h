/**
 * @file asm_device_interrupt_handler_wrapper.h
 * @brief Interface for device interrupt handler assembly wrapper
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#ifndef KERN_ASM_DEVICE_INTERRUPT_HANDLER_WRAPPER_H_
#define KERN_ASM_DEVICE_INTERRUPT_HANDLER_WRAPPER_H_

/**
 * @brief The assembly wrapper for the timer interrupt handler
 */
void asm_timer_interrupt_handler_wrapper();
void asm_keyboard_handler_wrapper();

#endif /* KERN_ASM_DEVICE_INTERRUPT_HANDLER_WRAPPER_H_ */
