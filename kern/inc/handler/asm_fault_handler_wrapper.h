/**
 * @file asm_fault_handler_wrapper.h
 * @brief Interface for the fault handler assembly wrapper
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#ifndef KERN_ASM_FAULT_HANDLER_WRAPPER_H_
#define KERN_ASM_FAULT_HANDLER_WRAPPER_H_

/**
 * @brief The assembly wrapper for page fault handler
 */
void asm_page_fault_handler_wrapper();

/**
 * @brief The assembly wrapper for general protection handler
 */
void asm_gp_fault_handler_wrapper();

#endif /* KERN_ASM_FAULT_HANDLER_WRAPPER_H_ */
