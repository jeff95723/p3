/**
 * @file asm_syscall_handler_wrapper.h
 * @brief Interface for the system call handler assembly wrapper
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#ifndef KERN_ASM_SYSCALL_HANDLER_WRAPPER_H_
#define KERN_ASM_SYSCALL_HANDLER_WRAPPER_H_

/**
 * @brief The assembly wrapper for the gettid system call
 */
void asm_gettid_syscall_wrapper();

/**
 * @brief The assembly wrapper for the fork system call
 */
void asm_fork_syscall_wrapper();

/**
 * @brief The assembly wrapper for the exec system call
 */
void asm_exec_syscall_wrapper();

/**
 * @brief The assembly wrapper for the halt system call
 */
void asm_halt_syscall_wrapper();

/**
 * @brief The assembly wrapper for the thread_fork system call
 */
void asm_thread_fork_syscall_wrapper();

/**
 * @brief The assembly wrapper for the set_status system call
 */
void asm_set_status_syscall_wrapper();

/**
 * @brief The assembly wrapper for the vanish system call
 */
void asm_vanish_syscall_wrapper();

/**
 * @brief The assembly wrapper for the wait system call
 */
void asm_wait_syscall_wrapper();

/**
 * @brief The assembly wrapper for the readline system call
 */
void asm_readline_syscall_wrapper();

/**
 * @brief The assembly wrapper for the print system call
 */
void asm_print_syscall_wrapper();

/**
 * @brief The assembly wrapper for the get cursor position system call
 */
void asm_get_cursor_pos_syscall_wrapper();

/**
 * @brief The assembly wrapper for the set cursor position system call
 */
void asm_set_cursor_pos_syscall_wrapper();

/**
 * @brief The assembly wrapper for the print system call
 */
void asm_set_term_color_syscall_wrapper();


/**
 * @brief The assembly wrapper for the new_pages system call
 */
void asm_new_pages_syscall_wrapper();

/**
 * @brief The assembly wrapper for the remove_pages system call
 */
void asm_remove_pages_syscall_wrapper();
#endif /* KERN_ASM_SYSCALL_HANDLER_WRAPPER_H_ */
