/**
 * @file task_mgr.h
 * @brief The interface for the task manager module
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#ifndef KERN_TASK_MGR_H_
#define KERN_TASK_MGR_H_

#include "../util/variable_queue.h"
#include "task_defines.h"
#include "../util/asm_utils.h"

/**
 * Get the base address of the current thread struct with given esp
 */
#define GET_THREAD_INFO(p) (((unsigned int) p) & 0xFFFFE000)

/**
 * Get the current thread struct
 */
#define GET_CURRENT_THREAD() ((thread_t *) GET_THREAD_INFO(get_esp()))

/**
 * Get the current task of the current running thread
 */
#define GET_CURRENT_TASK() ((GET_CURRENT_THREAD())->task)


extern task_mgr_t task_manager;

void task_init();

task_t *create_task();

thread_t *create_thread(task_t *task);

void destroy_task(task_t *task);

void destroy_thread(thread_t *thread);

void thread_vanish(thread_t *thread);

#endif /* KERN_TASK_MGR_H_ */
