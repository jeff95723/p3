/**
 * @file task_defines.h
 * @brief A header file containing macros for task manager module
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#ifndef KERN_TASK_DEFINES_H_
#define KERN_TASK_DEFINES_H_

#include <stdint.h>
#include "../util/kmutex.h"
#include "../util/kcond.h"
#include "../util/variable_hash_table.h"
#include "../util/variable_queue.h"


typedef struct task_manager_struct task_mgr_t;
typedef struct task_struct task_t;
typedef struct thread_struct thread_t;
typedef struct new_page_call_struct np_call_t;
typedef enum task_state task_state;
typedef enum thread_state thread_state;
Q_NEW_HEAD(task_queue_t, task_struct);
Q_NEW_HEAD(thread_queue_t, thread_struct);
Q_NEW_HEAD(np_list_t, new_page_call_struct);


HT_NEW_HEAD(task_hash_table, task_queue_t, int, task_struct);
HT_NEW_HEAD(thread_hash_table, thread_queue_t, int, thread_struct);

////////////////////////////////////////////////////////////////////////////////
/// Task & Thread structure
////////////////////////////////////////////////////////////////////////////////
enum task_state {
        TASK_RUNNING,
        TASK_INTERRUPTIBLE,
        TASK_UNINTERRUPTIBLE,
        TASK_STOPPED,
        TASK_ZOMBIE,
        TASK_DEAD
};

enum thread_state {
        THREAD_RUNNING,
        THREAD_SLEEP,
        THREAD_DESCHEDULED,
        THREAD_SUSPENDED,
        THREAD_DEAD
};

struct new_page_call_struct {
        void *base;
        int len;
        Q_NEW_LINK(new_page_call_struct) new_pages_calls;
};

struct task_manager_struct {
        int next_tid;                   // tid for the next thread creation
        int switching;                  // Indicator for ongoing context switch
        kmutex_t mutex;                 // task manager's mutex lock
        kmutex_t print_mutex;           // kernel's print mutex lock
        task_t *idle_task;              // pointer to idle task
        task_t *init_task;              // pointer to init task
        task_queue_t task_queue;        // task queue for traversal
        thread_queue_t runqueue;        // run queue for scheduler
        task_hash_table task_ht;        // (pid, task_t) hash table
        thread_hash_table thread_ht;    // (tid, thread_t) hash table
};

struct task_struct {
        int pid;
        int exit_status;
        task_state state;
        kmutex_t mutex;
        kcond_t cv;

        // Parent task.
        task_t *parent;

        // Task queue storing the children task created by fork
        task_queue_t children;

        // Task queue storing terminated children task
        task_queue_t dead_children;

        // Link name for children queue in parent task
        Q_NEW_LINK(task_struct) sibling;
        // Link name for dead children queue in parent task
        Q_NEW_LINK(task_struct) dead_sibling;
        // Link name for task queue in task manager
        Q_NEW_LINK(task_struct) iterator;
        // Link name for task hash table in task manager
        HT_CHAIN_LINK(task_struct) chain;

        // List of new_pages calls
        np_list_t np_calls;


        // Private address space
        void *dir_table;

        // thread management
        thread_t *thread;
        thread_queue_t thread_queue;
};

struct thread_struct {
        int tid;
        thread_state state;
        uint32_t esp;

        // Pointer to the owner task
        task_t *task;

        // Link name for run queue in task manager
        Q_NEW_LINK(thread_struct) runnable;
        // Link name for thread queue in task_t
        Q_NEW_LINK(thread_struct) iterator;
        // Link name for mutex waiting queue
        Q_NEW_LINK(thread_struct) mutex_wait;
        // Link name for cv waiting queue
        Q_NEW_LINK(thread_struct) cond_wait;
        // Link name for thread hash table in task manager
        HT_CHAIN_LINK(thread_struct) chain;
};


////////////////////////////////////////////////////////////////////////////////
/// Thread Kernel Stack Layout
////////////////////////////////////////////////////////////////////////////////

/*
 * The kernel stack of a thread is defined as a union of three structures
 *
 * Each thread kernel stack has a size of two pages
 *
 * The thread info structure is stored at the stack top (stack low addr)
 *
 * The register values for different purposes will be stored at stack bottom
 * with different layout.
 *
 * There are two layouts: syscall_stack & ctxt_switch_stack
 *
 * syscall_stack:
 *                **************************** <-- iret_stack
 *                *          %ss             *
 *                ****************************
 *                *          %esp            *
 *                ****************************
 *                *          %eflag          *
 *                ****************************
 *                *          %cs             *
 *                ****************************
 *                *          %eip            *
 *                **************************** <-- general_reg
 *                *          %eax            *
 *                ****************************
 *                *          %ebx            *
 *                ****************************
 *                *          %ecx            *
 *                ****************************
 *                *          %edx            *
 *                ****************************
 *                *          %edi            *
 *                ****************************
 *                *          %esi            *
 *                ****************************
 *                *          %ebp            *
 *                ****************************
 *                *           ...            *
 *                **************************** <-- thread info struct
 *                *        thread_t          *
 *                ****************************
 *
 * ctxt_switch_stack:
 *                **************************** <-- iret_stack
 *                *          %ss             *
 *                ****************************
 *                *          %esp            *
 *                ****************************
 *                *          %eflag          *
 *                ****************************
 *                *          %cs             *
 *                ****************************
 *                *          %eip            *
 *                **************************** <-- return address
 *                *         rtn_addr         *
 *                **************************** <-- segment_reg
 *                *          %ds             *
 *                ****************************
 *                *          %es             *
 *                ****************************
 *                *          %fs             *
 *                ****************************
 *                *          %gs             *
 *                **************************** <-- general_reg
 *                *          %eax            *
 *                ****************************
 *                *          %ebx            *
 *                ****************************
 *                *          %ecx            *
 *                ****************************
 *                *          %edx            *
 *                ****************************
 *                *          %edi            *
 *                ****************************
 *                *          %esi            *
 *                ****************************
 *                *          %ebp            *
 *                ****************************
 *                *           ...            *
 *                **************************** <-- thread info struct
 *                *        thread_t          *
 *                ****************************
 */

typedef struct {
        uint32_t eip;
        uint32_t cs;
        uint32_t eflag;
        uint32_t esp;
        uint32_t ss;
} iret_stack;

typedef struct {
        uint32_t ebp;
        uint32_t esi;
        uint32_t edi;
        uint32_t edx;
        uint32_t ecx;
        uint32_t ebx;
        uint32_t eax;
} general_reg;

typedef struct {
        uint32_t gs;
        uint32_t fs;
        uint32_t es;
        uint32_t ds;
} segment_reg;

typedef struct {
        unsigned long stack[2036];
        general_reg greg;
        iret_stack iret;
} syscall_stack;

typedef struct {
        unsigned long stack[2031];
        general_reg greg;
        segment_reg sreg;
        unsigned long rtn_addr;
        iret_stack iret;
} ctxt_switch_stack;

typedef union {
        thread_t thread;
        syscall_stack sys_stack;
        ctxt_switch_stack ctxt_stack;
} thread_stack;

#endif /* KERN_TASK_DEFINES_H_ */
