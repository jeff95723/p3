/**
 * @file kmutex.h
 * @brief Interface for the kernel mutex
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#ifndef KERN_KMUTEX_H_
#define KERN_KMUTEX_H_

#include "variable_queue.h"


typedef struct kmutex_struct kmutex_t;
_Q_NEW_HEAD(kmutex_queue_t, thread_struct);

struct kmutex_struct {
        int active;                     // indicator of the lock activity
        int owner_id;                   // tid of the owner thread
        void *owner;
        kmutex_queue_t waiting_queue;   // mutex waiting queue
};

void kmutex_init(kmutex_t *mutex);

void kmutex_destroy(kmutex_t *mutex);

void kmutex_lock(kmutex_t *mutex);

void kmutex_unlock(kmutex_t *mutex);

int kmutex_is_locked(kmutex_t *mutex);

#endif /* KERN_KMUTEX_H_ */
