/**
 * @file logger.h
 * @brief The interface for the logger tool
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#ifndef KERN_LOGGER_H_
#define KERN_LOGGER_H_

#define LOGGER_OFF 0
#define ERROR_LEVEL 1
#define WARNING_LEVEL 2
#define INFO_LEVEL 3
#define DEBUG_LEVEL 4
#define VERBOSE_LEVEL 5

#define MSG_BUF_LENGTH (256)

typedef struct logger_struct logger_t;
struct logger_struct {
        int level;                      // Current level of logging message
        void (*d)(char *msg, ...);      // Debug message logger
        void (*e)(char *msg, ...);      // Error message logger
        void (*i)(char *msg, ...);      // Info message logger
        void (*v)(char *msg, ...);      // Verbose message logger
        void (*w)(char *msg, ...);      // Warning message logger
};

extern logger_t Log;

void logger_init(int level);

void d(char *msg, ...);

void e(char *msg, ...);

void i(char *msg, ...);

void v(char *msg, ...);

void w(char *msg, ...);

#endif /* KERN_LOGGER_H_ */
