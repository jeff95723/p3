/*
 * input_stream.h
 *
 *  Created on: Nov 11, 2015
 *      Author: jianfeil
 */

#ifndef KERN_INC_UTIL_INPUT_STREAM_H_
#define KERN_INC_UTIL_INPUT_STREAM_H_

#include <stdint.h>
#include "kmutex.h"
#include "kcond.h"
#include "variable_queue.h"

#define DATA_BUFLEN 256

typedef struct stream_struct stream_t;
typedef struct stream_data_struct stream_data_t;

Q_NEW_HEAD(stream_data_queue, stream_data_struct);

struct stream_data_struct {
        char buf[DATA_BUFLEN];
        int read_index;
        int write_index;
        Q_NEW_LINK(stream_data_struct) stream_data;
};

struct stream_struct {
        kmutex_t mutex;
        kcond_t cond;
        int lines;
        stream_data_queue data;
};

extern stream_t input_stream;

void input_stream_init();

void input_stream_add(char c);

int input_stream_consume();

int has_pending_readline();

int input_stream_empty();


#endif /* KERN_INC_UTIL_INPUT_STREAM_H_ */
