/**
 * @file kcond.h
 * @brief The interface for the kernel condition variable module
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#ifndef KERN_INC_UTIL_KCOND_H_
#define KERN_INC_UTIL_KCOND_H_

#include "variable_queue.h"
#include "kmutex.h"

typedef struct kcond_struct kcond_t;
_Q_NEW_HEAD(kcond_queue_t, thread_struct);

struct kcond_struct {
        int active;                     // indicator for the cv activity
        kmutex_t mutex;                 // internal mutex lock
        kcond_queue_t waiting_queue;    // waiting queue for threads
};

void kcond_init(kcond_t *cv);

void kcond_destroy(kcond_t *cv);

void kcond_wait(kcond_t *cv, kmutex_t *mp);

void kcond_signal(kcond_t *cv);

void kcond_broadcast(kcond_t *cv);

#endif /* KERN_INC_UTIL_KCOND_H_ */
