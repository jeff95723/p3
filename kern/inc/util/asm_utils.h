/**
 * @file asm_utils.h
 * @brief Interface for assembly utility module
 * @author Jianfei Liu (jianfeil)
 * @bug No known bug
 */

#ifndef KERN_ASM_UTILS_H_
#define KERN_ASM_UTILS_H_

/**
 * @brief Get the address the current esp points to
 */
void *get_esp();

/**
 * @brief A wrapper that simple invokes iret
 */
void iret_wrapper();

/**
 * @brief An atomic process that fetches the current value and increment it
 * @param value The pointer of the value to be fetched and incremented
 * @return The current value
 */
int fetch_and_add(int *value);

#endif /* KERN_ASM_UTILS_H_ */
