/**
 * @file syscall_utils.h
 * @brief Interface for system call utilities
 * @author Jianfei Liu (jianfeil)
 * @bug No known bug
 */

#ifndef KERN_INC_UTIL_SYSCALL_UTILS_H_
#define KERN_INC_UTIL_SYSCALL_UTILS_H_

int mem_valid(void *p);

int arg_packet_mem_valid(void * p, int size);

int mem_available(void *p, int size);

int prog_name_valid(char *name);

#endif /* KERN_INC_UTIL_SYSCALL_UTILS_H_ */
