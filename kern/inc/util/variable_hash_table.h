/**
 * @file variable_hash_table.h
 * @brief Implementation for the hash table data structure
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#ifndef KERN_INC_VARIABLE_HASH_TABLE_H_
#define KERN_INC_VARIABLE_HASH_TABLE_H_

#include "variable_queue.h"

#define CHAIN_INDEX_FROM_KEY(HT_HEAD, HT_KEY)                   \
        (HT_HEAD)->hash(HT_KEY) % ((HT_HEAD)->capacity)

#define CHAIN_INDEX_FROM_ELEM(HT_HEAD, HT_ELEM)                 \
        (HT_HEAD)->hash((HT_HEAD)->elem_key(HT_ELEM)) % (HT_HEAD)->capacity

#define HT_MUTEX_INIT(HT_HEAD) kmutex_init(&((HT_HEAD)->mutex))

#define HT_MUTEX_DESTROY(HT_HEAD) kmutex_destroy(&((HT_HEAD)->mutex))

#define HT_MUTEX_LOCK(HT_HEAD) kmutex_lock(&((HT_HEAD)->mutex))

#define HT_MUTEX_UNLOCK(HT_HEAD) kmutex_unlock(&((HT_HEAD)->mutex))

#define HT_CHAIN_HEAD(HT_CHAIN_HEAD_TYPE, HT_ELEM_TYPE)         \
        _Q_NEW_HEAD(HT_CHAIN_HEAD_TYPE, HT_ELEM_TYPE)

#define HT_CHAIN_LINK(HT_ELEM_TYPE) Q_NEW_LINK(HT_ELEM_TYPE)

#define HT_NEW_HEAD(HT_HEAD_TYPE,HT_CHAIN_HEAD_TYPE,HT_KEY_TYPE,HT_ELEM_TYPE)  \
        typedef struct {                                                       \
                int size;                                                      \
                int capacity;                                                  \
                kmutex_t mutex;                                                \
                HT_CHAIN_HEAD_TYPE **chain;                                    \
                HT_KEY_TYPE (*elem_key)(struct HT_ELEM_TYPE *e);               \
                int (*key_cmp)(HT_KEY_TYPE k1, HT_KEY_TYPE k2);                \
                unsigned int (*hash)(HT_KEY_TYPE k);                           \
        } HT_HEAD_TYPE

#define HT_INIT_HEAD(HT_HEAD, HT_CAPACITY, HT_CHAIN,            \
                     HT_ELEM_KEY, HT_KEY_CMP, HT_HASH)          \
        (HT_HEAD)->size = 0;                                    \
        (HT_HEAD)->capacity = HT_CAPACITY;                      \
        (HT_HEAD)->chain = HT_CHAIN;                            \
        (HT_HEAD)->elem_key = HT_ELEM_KEY;                      \
        (HT_HEAD)->key_cmp = HT_KEY_CMP;                        \
        (HT_HEAD)->hash = HT_HASH;                              \
        HT_MUTEX_INIT(HT_HEAD)


#define HT_INIT_CHAIN(HT_HEAD)                                  \
        int i = 0;                                              \
        for (i = 0; i < (HT_HEAD)->capacity; i++) {             \
                _Q_INIT_HEAD((HT_HEAD)->chain[i]);              \
        }

#define HT_INIT_ELEM(HT_ELEM, LINK_NAME)                \
        (HT_ELEM)->LINK_NAME.next = NULL;               \
        (HT_ELEM)->LINK_NAME.prev = NULL;

#define HT_LOOKUP(HT_HEAD, HT_KEY, HT_ELEM, LINK_NAME)                         \
        HT_MUTEX_LOCK(HT_HEAD);                                                \
        Q_FOREACH(HT_ELEM,                                                     \
                  ((HT_HEAD)->chain[CHAIN_INDEX_FROM_KEY(HT_HEAD, HT_KEY)]),   \
                  LINK_NAME) {                                                 \
                if ((HT_HEAD)->key_cmp((HT_HEAD)->elem_key(HT_ELEM), HT_KEY)) {\
                        break;                                                 \
                }                                                              \
        }                                                                      \
        HT_MUTEX_UNLOCK(HT_HEAD)

#define HT_REMOVE(HT_HEAD, HT_KEY, HT_ELEM, LINK_NAME)                         \
        HT_MUTEX_LOCK(HT_HEAD);                                                \
        Q_FOREACH(HT_ELEM,                                                     \
                  ((HT_HEAD)->chain[CHAIN_INDEX_FROM_KEY(HT_HEAD, HT_KEY)]),   \
                  LINK_NAME) {                                                 \
                if ((HT_HEAD)->key_cmp((HT_HEAD)->elem_key(HT_ELEM), HT_KEY)) {\
                        _Q_REMOVE(((HT_HEAD)->chain[                           \
                                CHAIN_INDEX_FROM_KEY(HT_HEAD, HT_KEY)]),       \
                                  HT_ELEM,                                     \
                                  LINK_NAME);                                  \
                        (HT_HEAD)->size--;                                     \
                        break;                                                 \
                }                                                              \
        }                                                                      \
        HT_MUTEX_UNLOCK(HT_HEAD)

#define HT_INSERT(HT_HEAD, HT_ELEM, LINK_NAME)                               \
        HT_MUTEX_LOCK(HT_HEAD);                                              \
        _Q_INSERT_FRONT(                                                     \
                ((HT_HEAD)->chain[CHAIN_INDEX_FROM_ELEM(HT_HEAD, HT_ELEM)]), \
                HT_ELEM,                                                     \
                LINK_NAME);                                                  \
        (HT_HEAD)->size++;                                                   \
        HT_MUTEX_UNLOCK(HT_HEAD)




#endif /* KERN_INC_VARIABBLE_HASH_TABLE_H_ */
