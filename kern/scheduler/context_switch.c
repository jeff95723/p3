/**
 * @file context_switch.c
 * @brief The implementation for the context switch module
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#include "../inc/scheduler/context_switch.h"

#include <cr.h>
#include <syscall.h>
#include <stddef.h>

#include "../inc/task/task_mgr.h"
#include "../inc/util/asm_utils.h"
#include "../inc/util/logger.h"

/**
 * @brief Lock up the context switch procedure
 *        Make sure only one context switch procedure is being processed
 */
void context_switch_lock() {
        task_manager.switching = 1;
}

/**
 * @brief Unlock the context switch procedure
 *        Either the timer or other device can trigger context switch
 */
void context_switch_unlock() {
        task_manager.switching = 0;
}

/**
 * @brief The context switch handler to be invoked in context_switch
 * @param thread The thread to switched to
 */
void context_switch_handler(thread_t *switch_from, thread_t *switch_to) {
        if (switch_from == NULL) {
                Log.e("Switch from a NULL thread.");
                return;
        }
        if (switch_to == NULL) {
                Log.e("Switch to a NULL thread.");
                return;
        }
        if (switch_from->state == THREAD_DEAD) {
                destroy_thread(switch_from);
        }

        // change page directory table & set esp0
        set_cr3((uint32_t) switch_to->task->dir_table);
        set_esp0((uint32_t)(switch_to) + 2 * PAGE_SIZE);
}
