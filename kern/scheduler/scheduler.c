/**
 * @file scheduler.c
 * @brief Implementation for the scheduler module
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#include "../inc/scheduler/scheduler.h"

#include <stddef.h>

#include "../inc/task/task_mgr.h"
#include "../inc/util/variable_queue.h"
#include "../inc/util/logger.h"

/**
 * @brief Get the next thread to run from run queue
 * @return The thread struct pointer of the next thread to be run
 */
thread_t *next_thread() {
        thread_queue_t *runqueue = &(task_manager.runqueue);
        thread_t *next;
        if (Q_EMPTY(runqueue)) {
                // TODO: return idle thread
                return task_manager.idle_task->thread;
        }
        if (Q_SIZE(runqueue) == 1) {
                // return the first thread in run queue
                next = Q_GET_FRONT(runqueue);
                return next;
        }

        // put current thread to the back of run queue
        thread_t *curr;
        _Q_REMOVE_FRONT(&task_manager.runqueue, curr, runnable);
        _Q_INSERT_TAIL(&task_manager.runqueue, curr, runnable);
        next = Q_GET_FRONT(&task_manager.runqueue);

        return next;
}

/**
 * @brief Add the given thread to run queue
 * @param thread The thread to be inserted into run queue
 */
void insert_runnable_thread(thread_t *thread) {
        if (thread && thread->state != THREAD_RUNNING) {
                thread->state = THREAD_RUNNING;
                Q_INIT_ELEM(thread, runnable);
                _Q_INSERT_TAIL(&(task_manager.runqueue), thread, runnable);
        } else {
                Log.d("Inserting a null or running thread to run queue.");
        }
}

/**
 * @brief Remove the given thread from run queue
 * @param thread The thread to be removed from run queue
 */
void remove_runnable_thread(thread_t *thread) {
        if (thread && thread->state == THREAD_RUNNING) {
                thread->state = THREAD_SUSPENDED;
                _Q_REMOVE(&(task_manager.runqueue), thread, runnable);
        } else {
                Log.d("Removing a null or non-running thread from run queue.");
        }
}
