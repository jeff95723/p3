/**
 * @file context_switch.S
 * @brief The assembly implementation for context switch
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

.global context_switch

context_switch:
    // store registers
    pushl %ds
    pushl %es
    pushl %fs
    pushl %gs
    pushl %eax
    pushl %ebx
    pushl %ecx
    pushl %edx
    pushl %edi
    pushl %esi
    pushl %ebp

    // get current thread struct with current esp
    movl %esp, %edx
    andl $0xffffe000, %edx          // edx = switch_from (current thread)

    // store esp to current thread
    movl %esp, 8(%edx)              // switch_from->esp = esp

    // get new thread from argument
    movl 48(%esp), %ebx             // ebx = switch_to (new thread)
    movl 8(%ebx), %esp              // esp = switch_to->esp

    // invoke context switch handler
    pushl %ebx                      // push switch_to on stack
    pushl %edx                      // push switch_from on stack
    call context_switch_handler
    popl %edx
    popl %ebx

    // restore registers after switch back
    popl %ebp
    popl %esi
    popl %edi
    popl %edx
    popl %ecx
    popl %ebx
    popl %eax
    popl %gs
    popl %fs
    popl %es
    popl %ds

    call context_switch_unlock      // unlock context switch procedure

    ret
