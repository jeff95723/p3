/** @file kernel.c
 *  @brief An initial kernel.c
 *
 *  You should initialize things in kernel_main(),
 *  and then run stuff.
 *
 *  @author Harry Q. Bovik (hqbovik)
 *  @author Fred Hacker (fhacker)
 *  @bug No known bugs.
 */

#include <common_kern.h>

/* libc includes. */
#include <stdio.h>
#include <simics.h>                 /* lprintf() */

/* multiboot header file */
#include <multiboot.h>              /* boot_info */

/* x86 specific includes */
#include <x86/asm.h>                /* enable_interrupts() */

#include <eflags.h>

#include <simics.h>
#include <syscall.h>
#include <malloc_internal.h>

#include <cr.h>
#include <console.h>

#include "../inc/device_driver/timer_device_driver.h"
#include "../inc/mem/program_loader.h"
#include "../inc/mem/run_program.h"
#include "../inc/mem/frame_alloc.h"
#include "../inc/mem/vm.h"
#include "../inc/handler/idt_init.h"
#include "../inc/handler/keyboard_interrupt_handler.h"
#include "../inc/scheduler/scheduler.h"
#include "../inc/task/task_mgr.h"
#include "../inc/util/logger.h"
#include "../inc/handler/fault_handler.h"
#include "../inc/util/asm_utils.h"

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(mbinfo_t *mbinfo, int argc, char **argv, char **envp) {

        lprintf("Hello from a brand new kernel!");

        // TODO: Initialize IDT
        kern_timer_init();
        keyboard_handler_init();
        init_idt_entries();

        // TODO: Initialize everything (task_mgr, timer, paging, logger ...)
        task_init();
        logger_init(ERROR_LEVEL);

        // TODO: Clear console
        clear_console();

        // TODO: Initialize physical frame allocator
        frame_alloc_init();

        // TODO: Build initial page directory. Direct map kernel virtual memory
        init_paging();
        void *kern_table = new_directory_table_with_id();
        set_cr3((uint32_t) kern_table);
        enable_paging();

        // TODO: Create and load 'idle' task
        task_manager.idle_task = load_program("idle");

        // TODO: Create and load 'init' task
        void *stack_addr;
        void *entry_addr;
        if (load_elf("init", kern_table, &stack_addr, &entry_addr) < 0) {
                return -1;
        }

        uint32_t eflag = get_eflags();
        eflag |= EFL_RESV1;
        eflag |= EFL_IF;
        eflag &= (~EFL_IOPL_RING3);
        eflag &= (~EFL_AC);

        task_manager.init_task = create_task();
        task_manager.init_task->dir_table = kern_table;

        // TODO: Set first thread running
        set_esp0((uint32_t) (task_manager.init_task->thread) + 2 * PAGE_SIZE);
        set_cr3((uint32_t) (task_manager.init_task->dir_table));
        insert_runnable_thread(task_manager.init_task->thread);
        run_program(kern_table, stack_addr, entry_addr, (void *) eflag);

        while (1) {
                continue;
        }

        return 0;
}
