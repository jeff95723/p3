/**
 * @file miscellaneous_system_call.c
 * @brief Implementation for mescellaneous system calls, including
 *      readfile()
 *      halt()
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#include "../inc/syscall/system_call.h"

#include <simics.h>

void readfile_syscall(void) {

}

void halt_syscall(void) {
        sim_halt();
}
