/*
 * fork_system_call.c
 *
 */

#include "../inc/syscall/system_call.h"

#include <stddef.h>
#include <seg.h>
#include <asm.h>
#include <string.h>
#include <malloc_internal.h>
#include <cr.h>
#include <simics.h>

#include "../inc/mem/vm.h"
#include "../inc/mem/run_program.h"
#include "../inc/mem/program_loader.h"
#include "../inc/scheduler/context_switch.h"
#include "../inc/scheduler/scheduler.h"
#include "../inc/task/task_mgr.h"
#include "../inc/util/asm_utils.h"
#include "../inc/util/logger.h"
#include "../inc/util/variable_queue.h"


void fork_syscall() {
        // First check if eligible for fork.
        thread_t *old_thread = GET_CURRENT_THREAD();
        task_t *old_task = old_thread->task;
        int thr_count = old_task->thread_queue.size;

        syscall_stack *old_st = &(((thread_stack *) old_thread)->sys_stack);

        // Only those with a single thread is available for fork
        if (thr_count != 1) {
                Log.e("Fork failed: thread count is not 1.");
                old_st->greg.eax = -1;
                return;
        }

        // Create a new task.
        task_t *new_task = create_task();

        // Set parent & add to children queue
        new_task->parent = old_task;
        Q_INIT_ELEM(new_task, sibling);
        Q_INSERT_TAIL(&(old_task->children), new_task, sibling);
        thread_t *new_thread = new_task->thread;
        ctxt_switch_stack *new_st = &(((thread_stack*) new_thread)->ctxt_stack);

        // Create failed, abort.
        if (!new_task) {
                Log.e("Fork failed: Failed to create new task.");
                old_st->greg.eax = -1;
                return;
        }

        // Copy over the kernel stack, make it ready for context switch
        new_st->iret = old_st->iret;
        new_st->sreg.gs = SEGSEL_USER_DS;
        new_st->sreg.fs = SEGSEL_USER_DS;
        new_st->sreg.es = SEGSEL_USER_DS;
        new_st->sreg.ds = SEGSEL_USER_DS;
        new_st->greg = old_st->greg;

        // Set up return value for both child and parent
        new_st->greg.eax = 0;
        old_st->greg.eax = new_thread->tid;
        new_thread->esp = (uint32_t) &(new_st->greg.ebp);

        // Next instruction should be iret boys
        new_st->rtn_addr = (unsigned long) iret_wrapper;

        // Create new table and make a copy of the old directory table.
        void *new_dt = new_directory_table_with_id();
        copy_dir_table(old_task->dir_table, new_dt, (void *) new_thread);
        new_task->dir_table = new_dt;

        // Add it to the scheduler run queue
        // Only add it to queue when shit if finished
        insert_runnable_thread(new_thread);

        return;
}
