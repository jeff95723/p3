/*
 * system_call.c
 *
 *  Created on: Oct 23, 2015
 *      Author: hongyul
 */

#include "../inc/syscall/system_call.h"

#include "../inc/task/task_mgr.h"
#include "../inc/util/asm_utils.h"


void gettid_syscall() {
        thread_t *thread_info = GET_CURRENT_THREAD();
        ((thread_stack *) thread_info)->sys_stack.greg.eax = thread_info->tid;
}
