/*
 * readline_system_call.c
 *
 *  Created on: Nov 12, 2015
 *      Author: liujianfei
 */


#include "../inc/syscall/system_call.h"

#include "../inc/task/task_mgr.h"
#include "../inc/scheduler/context_switch.h"
#include "../inc/scheduler/scheduler.h"
#include "../inc/util/syscall_utils.h"
#include "../inc/util/kmutex.h"
#include "../inc/util/kcond.h"
#include "../inc/util/input_stream.h"
#include "../inc/util/logger.h"


void readline_syscall() {
        thread_t *thread_info = GET_CURRENT_THREAD();
//        task_t *task = thread_info->task;
        syscall_stack *sys_st = &(((thread_stack *) thread_info)->sys_stack);

        // Check argument packet memory
        void *packet = (void *)sys_st->greg.esi;
        if (arg_packet_mem_valid(packet, 8) != 1) {
                Log.e("Readline failed: argument package address is invalid.");
                sys_st->greg.eax = -1;
                return;
        }
        void **argument_packet = (void **) sys_st->greg.esi;
        int len = (int) argument_packet[0];
        char *buf = (char *) argument_packet[1];

        if (arg_packet_mem_valid(buf, len) != 1) {
                Log.e("Readline failed: buf address not valid.");
                sys_st->greg.eax = -1;
                return;
        }

        // Check if len is too large
        kmutex_lock(&input_stream.mutex);
        while (input_stream.lines == 0) {
                kcond_wait(&input_stream.cond, &input_stream.mutex);
        }

        // Time to consume the stuff in input_stream
        int i;
        for (i = 0; i < len; i++) {
                int consumed = input_stream_consume();
                if (consumed == -1) {
                        Log.e("Readline failed: Readline was signaled but "
                                        "nothing was in the input stream.");
                        sys_st->greg.eax = (i+1);
                        // Failed to read from input stream
                        return;
                }

                char c = (char) consumed;
                buf[i] = c;
                if (c == '\n') {
                        if (i + 1 < len) {
                                // Just being nice, closing the string for you
                                buf[i+1] = '\0';
                        }
                        sys_st->greg.eax = (i+1);
                        return;
                }
        }
        sys_st->greg.eax = len;
        return;
}
