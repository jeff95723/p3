/**
 * @file life_cycle_system_call.c
 * @brief Implementation for life cycle system calls, including
 *      thread_fork()
 *      set_status()
 *      vanish()
 *      wait()
 *      task_vanish()
 * @author Hongyu Li (hongyul)
 *         Jianfei Liu (jianfeil)
 * @bug No known bug
 */

#include "../inc/syscall/system_call.h"

#include <stddef.h>
#include <seg.h>

#include "../inc/task/task_mgr.h"
#include "../inc/scheduler/scheduler.h"
#include "../inc/scheduler/context_switch.h"
#include "../inc/util/syscall_utils.h"
#include "../inc/util/logger.h"

void thread_fork_syscall(void) {
        // get current thread & task
        thread_t *current = GET_CURRENT_THREAD();
        task_t *task = current->task;
        syscall_stack *current_stack = &(((thread_stack *) current)->sys_stack);

        // create new thread
        thread_t *new = create_thread(task);
        if (!new) {
                Log.d("thread_fork(): failed to create thread.");
                current_stack->greg.eax = -1;
                return;
        }

        // set up new thread stack for context switch
        ctxt_switch_stack *new_stack = &(((thread_stack *) new)->ctxt_stack);
        new_stack->iret = current_stack->iret;
        new_stack->sreg.gs = SEGSEL_USER_DS;
        new_stack->sreg.fs = SEGSEL_USER_DS;
        new_stack->sreg.es = SEGSEL_USER_DS;
        new_stack->sreg.ds = SEGSEL_USER_DS;
        new_stack->greg = current_stack->greg;

        // Set up return value for old thread and new thread
        new_stack->greg.eax = 0;
        current_stack->greg.eax = new->tid;
        new->esp = (uint32_t) (&(new_stack->greg.ebp));

        // Next instruction for new thread should be iret
        new_stack->rtn_addr = (unsigned long) iret_wrapper;

        // Add thread to run queue
        context_switch_lock();
        insert_runnable_thread(new);
        context_switch_unlock();

        return;
}

void set_status_syscall(void) {
        thread_t *thread_info = GET_CURRENT_THREAD();
        syscall_stack *stack = &(((thread_stack *) thread_info)->sys_stack);

        // get argument from packet
        int status = (int) stack->greg.esi;

        // set exit status
        thread_info->task->exit_status = status;
}

void vanish_syscall(void) {
        Log.d("Enter vanish syscall");
        thread_t *thread = GET_CURRENT_THREAD();
        thread_vanish(thread);

        // context switch
        Log.d("Prepare context switch");
        context_switch_lock();
        thread_t *want_to_switch = next_thread();
        if (!want_to_switch) {
                Log.e("vanish(): next thread is null");
        }
        context_switch(want_to_switch);
        context_switch_unlock();
}

void wait_syscall(void) {
        Log.d("Wait(): Enter wait syscall");
        thread_t *thread_info = GET_CURRENT_THREAD();
        task_t *task = thread_info->task;
        syscall_stack *stack = &(((thread_stack *) thread_info)->sys_stack);

        // get argument from esi
        int *status_ptr = (int *) stack->greg.esi;

        // check argument validity
        // TODO: need to modify mem_valid to check writability
        if (status_ptr && (mem_valid(status_ptr) < 0)) {
                stack->greg.eax = -1;
                return;
        }

        Log.d("Wait(): end of validity check");

        // no child task to wait or no dead task, return -1
        kmutex_lock(&task->mutex);
        if (Q_EMPTY(&task->children) && Q_EMPTY(&task->dead_children)) {
                Log.e("Wait(): nothing to wait for.");
                stack->greg.eax = -1;
                kmutex_unlock(&task->mutex);
                return;
        }

        // wait for some children to terminate
        Log.d("Wait(): wait children to terminate");
        while(Q_EMPTY(&task->dead_children)) {
                kcond_wait(&task->cv, &task->mutex);
        }
        Log.d("Wait(): get exit child.");
        // get a terminated child from dead_children queue
        task_t *exit_child;
        Q_REMOVE_FRONT(&task->dead_children, exit_child, dead_sibling);
        // set exit status
        if (status_ptr) {
                *status_ptr = exit_child->exit_status;
        }

        // return the tid of the original thread of the terminated task
        stack->greg.eax = exit_child->pid;
        destroy_task(exit_child);
        kmutex_unlock(&task->mutex);
        return;
}

void task_vanish_syscall(void) {
        Log.i("Don't need to implement task_vanish!");
}
