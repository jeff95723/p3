/*
 * console_io_syscall.c
 *
 *  Created on: Nov 12, 2015
 *      Author: jianfeil
 */

#include "../inc/syscall/system_call.h"

#include "../inc/task/task_mgr.h"
#include "../inc/scheduler/context_switch.h"
#include "../inc/scheduler/scheduler.h"
#include "../inc/util/syscall_utils.h"
#include "../inc/util/kmutex.h"
#include "../inc/util/kcond.h"
#include "../inc/util/input_stream.h"
#include "../inc/device_driver/console_driver.h"
#include "../inc/util/logger.h"

void print_syscall() {
        thread_t *thread_info = GET_CURRENT_THREAD();
//        task_t *task = thread_info->task;
        syscall_stack *sys_st = &(((thread_stack *) thread_info)->sys_stack);

        // Check argument packet memory
        void *packet = (void *)sys_st->greg.esi;
        if (arg_packet_mem_valid(packet, 8) != 1) {
                Log.e("Print failed: argument package address is invalid.");
                sys_st->greg.eax = -1;
                return;
        }
        void **argument_packet = (void **) sys_st->greg.esi;
        int len = (int) argument_packet[0];
        char *buf = (char *) argument_packet[1];

        if (arg_packet_mem_valid(buf, len) != 1) {
                Log.e("Print failed: buf address not valid.");
                sys_st->greg.eax = -1;
                return;
        }

        kmutex_lock(&task_manager.print_mutex);
        putbytes(buf, len);
        kmutex_unlock(&task_manager.print_mutex);
        // I return whatever the fuck i want
        sys_st->greg.eax = 0;
        return;
}

void set_term_color_syscall(void) {
        thread_t *thread_info = GET_CURRENT_THREAD();
//        task_t *task = thread_info->task;
        syscall_stack *sys_st = &(((thread_stack *) thread_info)->sys_stack);

        // Check argument packet memory
        int color = (int) sys_st->greg.esi;
        int res = console_set_term_color(color);
        sys_st->greg.eax = res;
        return;

}
void set_cursor_pos_syscall(void){
        thread_t *thread_info = GET_CURRENT_THREAD();
//        task_t *task = thread_info->task;
        syscall_stack *sys_st = &(((thread_stack *) thread_info)->sys_stack);

        // Check argument packet memory
        void *packet = (void *)sys_st->greg.esi;
        if (arg_packet_mem_valid(packet, 2 * sizeof(int)) != 1) {
                Log.e("Set cursor pos failed: argument package address is invalid.");
                sys_st->greg.eax = -1;
                return;
        }
        void **argument_packet = (void **) sys_st->greg.esi;
        int row = (int) argument_packet[0];
        int col = (int) argument_packet[1];

        int res = console_set_cursor(row, col);
        sys_st->greg.eax = res;
        return;
}
void get_cursor_pos_syscall(void) {
        thread_t *thread_info = GET_CURRENT_THREAD();
//        task_t *task = thread_info->task;
        syscall_stack *sys_st = &(((thread_stack *) thread_info)->sys_stack);

        // Check argument packet memory
        void *packet = (void *)sys_st->greg.esi;
        if (arg_packet_mem_valid(packet, 2 * sizeof(int *)) != 1) {
                Log.e("Get cursor pos failed: argument package address is invalid.");
                sys_st->greg.eax = -1;
                return;
        }
        void **argument_packet = (void **) sys_st->greg.esi;
        int* row= (int *) argument_packet[0];
        int* col= (int *) argument_packet[1];

        if (arg_packet_mem_valid(row, sizeof(int)) != 1) {
                Log.e("Get cursor pos failed: row address not valid.");
                sys_st->greg.eax = -1;
                return;
        }

        if (arg_packet_mem_valid(col, sizeof(int)) != 1) {
                Log.e("Get cursor pos failed: col address not valid.");
                sys_st->greg.eax = -1;
                return;
        }
        console_get_cursor(row, col);
        sys_st->greg.eax = 0;
        return;
}
