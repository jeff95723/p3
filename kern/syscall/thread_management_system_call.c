/**
 * @file thread_management_system_call.c
 * @brief Implementation for thread management system calls, including
 *      gettid()
 *      deschedule()
 *      make_runnable()
 *      get_ticks()
 *      sleep()
 *      swexn()
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#include "../inc/syscall/system_call.h"

#include <stddef.h>

#include "../inc/scheduler/scheduler.h"
#include "../inc/scheduler/context_switch.h"
#include "../inc/task/task_mgr.h"
#include "../inc/util/asm_utils.h"

void gettid_syscall() {
        thread_t *thread_info = GET_CURRENT_THREAD();
        ((thread_stack *) thread_info)->sys_stack.greg.eax = thread_info->tid;
}

void yield_syscall() {
        thread_t *thread_info = GET_CURRENT_THREAD();
        syscall_stack *stack = &(((thread_stack *) thread_info)->sys_stack);

        // TODO: get argument tid from esi
        int tid = (int) stack->greg.esi;

        // TODO: if tid == -1, context switch to next thread, wrtie 0 to eax
        if (tid == -1) {
                stack->greg.eax = 0;
                context_switch_lock();
                context_switch(next_thread);
                context_switch_unlock();
                return;
        }

        // TODO: if tid does not exist (check in hash table), write error code
        //       to eax
        thread_t *target = NULL;
        HT_LOOKUP(&task_manager.thread_ht, tid, target, chain);
        if (!target || target->state != THREAD_RUNNING) {
                stack->greg.eax = -1;
                return;
        }

        // TODO: if thread is suspended (by readline(), wait(), deschedule() ?),
        //       write error code to eax

        // TODO: context switch to target thread
        stack->greg.eax = 0;
        context_switch_lock();
        context_switch(target);
        context_switch_unlock();

        return;
}

void deschedule_syscall() {
        thread_t *thread_info = GET_CURRENT_THREAD();
        syscall_stack *stack = &(((thread_stack *) thread_info)->sys_stack);

        // TODO: get argument (int *reject)
        int *reject = (int *) stack->greg.esi;

        // TODO: check reject writable ? use mem_valid ?

        // TODO: if reject not a valid pointer, write error code to eax, return
        if (!reject) {
                stack->greg.eax = -1;
                return;
        }

        // TODO: if *reject != 0, write 0 to eax, return
        if (*reject != 0) {
                stack->greg.eax = 0;
                return;
        } else {
                stack->greg.eax = 0;
                context_switch_lock();
                thread_t *want_to_switch = next_thread();
                remove_runnable_thread(thread_info);
                context_switch(want_to_switch);
                context_switch_lock();
                return;
        }
}

void make_runnaable_syscall() {
        thread_t *thread_info = GET_CURRENT_THREAD();
        syscall_stack *stack = &(((thread_stack *) thread_info)->sys_stack);

        int tid = (int) stack->greg.esi;
}

void get_ticks_syscall() {
        thread_t *thread_info = GET_CURRENT_THREAD();
        syscall_stack *stack = &(((thread_stack *) thread_info)->sys_stack);
        stack->greg.eax = (uint32_t) (kern_timer_get_ticks());
        return;
}

void sleep_syscall() {
        thread_t *thread_info = GET_CURRENT_THREAD();
        syscall_stack *stack = &(((thread_stack *) thread_info)->sys_stack);

        int ticks = (int) stack->greg.esi;
}

void swexn_syscall() {
        thread_t *thread_info = GET_CURRENT_THREAD();
        syscall_stack *stack = &(((thread_stack *) thread_info)->sys_stack);

        void **argument_packet = (void **) stack->greg.esi;
        void *esp3 = argument_packet[0];
        swexn_handler_t eip = (swexn_handler_t) argument_packet[1];
        void *arg = argument_packet[2];
        ureg_t *newureg = (ureg_t *) argument_packet[3];
}
