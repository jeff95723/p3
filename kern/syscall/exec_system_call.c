/*
 * exec_system_call.c
 */

#include "../inc/syscall/system_call.h"

#include <stddef.h>
#include <seg.h>
#include <asm.h>
#include <string.h>
#include <malloc_internal.h>
#include <cr.h>
#include <simics.h>
#include <exec2obj.h>

#include "../inc/mem/vm.h"
#include "../inc/mem/run_program.h"
#include "../inc/mem/program_loader.h"
#include "../inc/scheduler/context_switch.h"
#include "../inc/scheduler/scheduler.h"
#include "../inc/task/task_mgr.h"
#include "../inc/util/asm_utils.h"
#include "../inc/util/syscall_utils.h"
#include "../inc/util/logger.h"


void exec_syscall() {
        // First check if eligible for fork.
        Log.e("Exec started.");
        thread_t *old_thread = GET_CURRENT_THREAD();
        task_t *old_task = old_thread->task;
        int thr_count = old_task->thread_queue.size;

        syscall_stack *old_st = &(((thread_stack *) old_thread)->sys_stack);


        // Only those with a single thread is available for fork
        if (thr_count != 1) {
                Log.e("Exec failed: thread count is not 1.");
                old_st->greg.eax = -1;
                return;
        }
        // Check packet address
        // TODO: Check if this address is in user space or kernel space (USE VM)
        if (!arg_packet_mem_valid((void *)old_st->greg.esi,
                        2 * sizeof(char *))) {
                Log.e("Exec failed: argument package address is invalid.");
                old_st->greg.eax = -1;
                return;
        }

        // TODO: Check arg packet memory permissions
        void **argument_packet = (void **) old_st->greg.esi;
        char *execname = (char *) argument_packet[0];
        char **argvec = (char **) argument_packet[1];

        // basic null check
        if (!(execname) || !(argvec)) {
                Log.e("Exec failed: argument packet count is invalid.");
                old_st->greg.eax = -1;
                return;
        }

        // Check if name is valid or not, return error.
        if (!prog_name_valid(execname)) {
                Log.e("Exec failed: Exec name is not valid.");
                old_st->greg.eax = -1;
                return;
        }


        // Create buffer to hold args, first get arg count
        int argc;
        for (argc = 0; argvec[argc]; argc++);
        char **argv = _malloc(argc * sizeof(char *));
        int i;
        for (i = 0; i < argc; i++) {
                argv[i] = _malloc(strlen(argvec[i]) + 1);
                strcpy(argv[i], argvec[i]);
        }

        // Throw away old stuff, create new page directory.
        // TODO: Recycle old dir table
        void *dir_table = new_directory_table_with_id();
        void *stack_addr;
        void *entry_addr;
        old_thread->task->dir_table = dir_table;
        char * name = _malloc(MAX_EXECNAME_LEN + 1);
        strncpy(name, execname, MAX_EXECNAME_LEN);
        name[MAX_EXECNAME_LEN] = '\0';
        set_cr3((uint32_t) old_task->dir_table);
        if (load_elf(name, dir_table, &stack_addr, &entry_addr) < 0) {
                // Failed to load, clean up
                Log.e("Exec failed: failed to load elf.");
                old_st->greg.eax = -1;
                for (i = 0; i < argc; i++) {
                        _free(argv[i]);
                }
                _free(argv);
                return;
        }
        Log.e("Exec finished loading elf.");

        /*
         *                                  0x00000000
         *
         *
         *              ...
         *
         * ------------------------------
         * |                            |
         * ------------------------------  <- esp
         * |      return addr           |
         * ------------------------------
         * |      argc                  |
         * ------------------------------
         * |      argv_user             |
         * ------------------------------
         * |      stack_hi              |
         * ------------------------------
         * |      stack_low             |
         * ------------------------------  <- ch_stack_addr (at first)
         * |      argv_user             |
         * ------------------------------
         * |      argv[0]               |
         * ------------------------------
         * |      argv[1]               |
         * ------------------------------
         *              ...
         * ------------------------------
         * |      argv[argc-1]          |
         * ------------------------------  <-ch_stack_addr (after copy)
         * |                            |
         * ------------------------------
         * |                            |
         * ------------------------------
         *              ...
         * ------------------------------
         * |                            |
         * ------------------------------
         * |                            |
         * ------------------------------  <- stack_addr
         *
         *              ...
         *
         *                                  0xFFFFFFFF
         */

        char **argv_user = (char **) ((char *)stack_addr - PAGE_SIZE);
        char *ch_stack_addr = (char *)stack_addr - PAGE_SIZE;
        ch_stack_addr += sizeof(char **) * argc;

        for (i = 0; i < argc; i++) {
                strcpy(ch_stack_addr, argv[i]);
                argv_user[i] = ch_stack_addr;
                ch_stack_addr += strlen(argv[i]) + 1;
        }

        for (i = 0; i < argc; i++) {
                _free(argv[i]);
        }
        _free(argv);


        int *esp = ((int *) ((char *) stack_addr - PAGE_SIZE)) - 5;
        esp[1] = argc;
        esp[2] = (int) argv_user;
        esp[3] = (int) stack_addr;
        esp[4] = (int) (STACK_START - (STACK_SIZE + 1) * PAGE_SIZE);

        Log.e("Exec finished setting up stack.");

        run_program(dir_table, (void *) esp, entry_addr,
                        (void *)old_st->iret.eflag);
}
