/**
 * @file memory_management_system_call.c
 * @brief Implementation for memory management system calls, including
 *      new_page()
 *      remove_page()
 *
 * @author Jeffrey Liu (jianfeil)
 */

#include "../inc/syscall/system_call.h"

#include <stddef.h>
#include <cr.h>
#include <syscall.h>
#include <malloc.h>

#include "../inc/task/task_defines.h"
#include "../inc/task/task_mgr.h"
#include "../inc/util/syscall_utils.h"
#include "../inc/util/variable_queue.h"
#include "../inc/util/logger.h"
#include "../inc/mem/vm.h"
#include "../inc/mem/frame_alloc.h"

void new_pages_syscall() {
        thread_t *thread_info = GET_CURRENT_THREAD();
        task_t *task = thread_info->task;
        syscall_stack *sys_st = &(((thread_stack *) thread_info)->sys_stack);

        // Check argument packet memory
        void *packet = (void *)sys_st->greg.esi;
        if (arg_packet_mem_valid(packet, sizeof(void *) + sizeof(int)) != 1) {
                Log.e("New_pages failed: argument package address is invalid.");
                sys_st->greg.eax = -1;
                return;
        }
        void **argument_packet = (void **) sys_st->greg.esi;
        void *base = (void *) argument_packet[0];
        int len = (int) argument_packet[1];

        // Length check
        if (!((len % PAGE_SIZE == 0) && ((len / PAGE_SIZE) > 0))) {
                Log.e("New_pages failed: length is invalid.");
                sys_st->greg.eax = -1;
                return;
        }

        // Memory overlap check
        if (!mem_available(base, len)) {
                Log.e("New_pages failed: Requested memory is not available.");
                sys_st->greg.eax = -1;
                return;
        }

        // Resources check
        np_call_t *call = malloc(sizeof(np_call_t));
        if ((call == NULL) || (available_frames() < (len / PAGE_SIZE))) {
                Log.e("New_pages failed: not enough resources available.");
                sys_st->greg.eax = -1;
                return;
        }

        // Allocation
        unsigned int addr = (unsigned int) base;
        int offset;
        for (offset = 0; offset < len; offset += PAGE_SIZE) {
                add_newpage((void *)get_cr3(), (void *)(addr + offset), 1, 1);
        }


        call->base = base;
        call->len = len;
        Q_INIT_ELEM(call, new_pages_calls);
        Q_INSERT_TAIL(&(task->np_calls), call, new_pages_calls);

        sys_st->greg.eax = 0;
        return;
}

void remove_pages_syscall() {
        thread_t *thread_info = GET_CURRENT_THREAD();
        task_t *task = thread_info->task;
        syscall_stack *sys_st = &(((thread_stack *) thread_info)->sys_stack);

        // Check argument packet memory
        void *base = (void *)sys_st->greg.esi;
        np_call_t *call;
        Q_FOREACH(call, &(task->np_calls), new_pages_calls) {
                if (call->base == base) {
                        unsigned int addr = (unsigned int) base;
                        int o;
                        for (o= 0; o< call->len; o+= PAGE_SIZE) {
                                remove_newpage((void *)get_cr3(),
                                                (void *)(addr + o));
                        }
                        Q_REMOVE(&(task->np_calls) ,call, new_pages_calls);
                        free(call);
                        sys_st->greg.eax = 0;
                        return;
                }
        }

        sys_st->greg.eax = -1;
        Log.e("Reomve_pages failed: corresponding new page call not found.");
        return;
}
