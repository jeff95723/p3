/** @file console_driver.c
 *
 *  @brief Implementation of the console-driver library.
 *
 *  @author Jeffrey Liu (jianfeil)
 */

#include <string.h>
#include <console.h>
#include <simics.h>
#include <stdio.h>
#include <asm.h>

#include "../inc/device_driver/console_driver.h"

int logical_row = -1;
int logical_col = -1;
int visible = -1;
char term_color = -1;

void set_hardware_cursor(int row, int col);
void scroll_down();
void initGlobals();

/**
 * @brief Initializes the globals if they are not done so.
 */
void initGlobals() {
    if ((logical_row == -1) || (logical_col == -1)) {
        unsigned int position;
        // cursor position low register
        outb(CRTC_IDX_REG, CRTC_CURSOR_LSB_IDX);
        uint8_t positionLowBits = inb(CRTC_DATA_REG);

        // cursor position high register
        outb(CRTC_IDX_REG, CRTC_CURSOR_MSB_IDX);
        uint8_t positionHighBits = inb(CRTC_DATA_REG);
        position = (positionHighBits << 8) + positionLowBits;
        logical_row = position / CONSOLE_WIDTH;
        logical_col = position % CONSOLE_WIDTH;
    }

    if (visible == -1) {
        visible = 1;
    }

    if (term_color == -1) {
        char *term_color_ptr;
        term_color_ptr = (char *) ((CONSOLE_MEM_BASE
                + 2 * (logical_row * (CONSOLE_WIDTH) + logical_col)) + 1);
        term_color = *term_color_ptr;
    }
}

int putbyte(char ch) {
    initGlobals();
    // If cursor already hidden, don't do anything
    switch (ch) {
        case '\n':
            logical_row++;
            logical_col = 0;
            break;
        case '\r':
            logical_col = 0;
            break;
        case '\b':
            if (logical_col != 0) {
                logical_col--;
                draw_char(logical_row, logical_col, ' ', term_color);
            } else if (logical_row != 0){
                    logical_row--;
                    logical_col = CONSOLE_WIDTH - 1;
                    draw_char(logical_row, logical_col, ' ', term_color);
            }
            break;
        default:
            draw_char(logical_row, logical_col, ch, term_color);
            logical_col++;
            if (logical_col == CONSOLE_WIDTH) {
                // Get to a new line
                logical_col = 0;
                logical_row++;
            }
            break;
    }
    if (logical_row >= CONSOLE_HEIGHT) {
        scroll_down();
    }
    if (visible) {
        set_hardware_cursor(logical_row, logical_col);
    }
    return (int) ch;
}

void putbytes(const char *s, int len) {
    initGlobals();
    if (s == NULL) return;
    int index;
    for (index = 0; index < len; index++) {
        putbyte(s[index]);
    }
}

int console_set_term_color(int color) {
    initGlobals();
    char *term_color_ptr;
    term_color = color;
    term_color_ptr = (char *) (CONSOLE_MEM_BASE
            + 2 * (logical_row * (CONSOLE_WIDTH) + logical_col) + 1);
    *term_color_ptr = color;
    return color;
}

void get_term_color(int *color) {
    if (color == NULL) return;
    initGlobals();
    *color = term_color;
}

int console_set_cursor(int row, int col) {
    initGlobals();
    if ((row < 0) || (col < 0) || (row >= CONSOLE_HEIGHT)
            || (col >= CONSOLE_WIDTH)) {
        return -1;
    }

    logical_row = row;
    logical_col = col;
    if (visible) {
        set_hardware_cursor(row, col);
    }
    return 0;
}

void console_get_cursor(int *row, int *col) {
    initGlobals();
    if ((row == NULL) || (col == NULL)) return;
    *row = logical_row;
    *col = logical_col;
    return;
}

void hide_cursor() {
    initGlobals();
    if (visible) {
        set_hardware_cursor(CONSOLE_HEIGHT, 0);
        visible = 0;
    }
}

void show_cursor() {
    initGlobals();
    if (!visible) {
        set_hardware_cursor(logical_row, logical_col);
        visible = 1;
    }
}

void clear_console() {
    int row, col;
    for (row = 0; row < CONSOLE_HEIGHT; row++) {
        for (col = 0; col < CONSOLE_WIDTH; col++) {
            draw_char(row, col, ' ', term_color);
        }
    }
    if (visible) {
        logical_row = 0;
        logical_col = 0;
    }
    initGlobals();
}

void draw_char(int row, int col, int ch, int color) {
    initGlobals();
    if ((row < 0) || (col < 0) || (row >= CONSOLE_HEIGHT)
            || (col >= CONSOLE_WIDTH)) {
        return;
    }
    char *term_color_ptr = (char *) ((CONSOLE_MEM_BASE
            + 2 * (row * (CONSOLE_WIDTH) + col)) + 1);
    char *term_char_ptr = (char *) (CONSOLE_MEM_BASE
            + 2 * (row * (CONSOLE_WIDTH) + col));
    *term_color_ptr = color;
    *term_char_ptr = ch;
}

char get_char(int row, int col) {
    initGlobals();
    char *term_char_ptr = (char *) (CONSOLE_MEM_BASE
            + 2 * (logical_row * (CONSOLE_WIDTH) + logical_col));
    return *term_char_ptr;
}

/**
 * @brief Scroll down the content from the terminal. Called when the test is
 * going to be out of the current console window.
 */
void scroll_down() {
    int row, col;
    void* src;
    void* dst;
    size_t rowSize = 2 * CONSOLE_WIDTH;
    for (row = 1; row < CONSOLE_HEIGHT; row++) {
        dst = (void *) (CONSOLE_MEM_BASE + 2 * (row * CONSOLE_WIDTH));
        src = (void *) (CONSOLE_MEM_BASE + 2 * ((row + 1) * CONSOLE_WIDTH));
        memcpy(dst, src, rowSize);
    }

    for (col = 0; col < CONSOLE_WIDTH; col++) {
        draw_char(CONSOLE_HEIGHT - 1, col, ' ', term_color);
    }
    logical_row--;

}

/**
 * Sets the hardware cursor to the given row and col, assumes the input is
 * always valid.
 * @param row The row to set the hardware cursor
 * @param col The col to set the hardware cursor
 */
void set_hardware_cursor(int row, int col) {
    if ((row >= 0) && (row <= CONSOLE_HEIGHT) && (col >= 0)
            && (col <= CONSOLE_WIDTH)) {
        unsigned int position = CONSOLE_WIDTH * row + col;
        outb(CRTC_IDX_REG, CRTC_CURSOR_LSB_IDX);
        outb(CRTC_DATA_REG, (position & 0xFF));

        // cursor position high register
        outb(CRTC_IDX_REG, CRTC_CURSOR_MSB_IDX);
        outb(CRTC_DATA_REG, ((position >> 8) & 0xFF));
    }
}
