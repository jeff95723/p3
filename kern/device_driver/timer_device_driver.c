/**
 * @file timer_device_driver.c
 * @brief Implementation for the timer device driver
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#include "../inc/device_driver/timer_device_driver.h"

#include <x86/timer_defines.h>
#include <x86/asm.h>


unsigned int timer_ticks;       // Number of timer ticks occurred

/**
 * @brief Initialize the timer device driver
 */
void kern_timer_init() {
        outb(TIMER_MODE_IO_PORT, TIMER_SQUARE_WAVE);
        outb(TIMER_RATE & 0xF, TIMER_PERIOD_IO_PORT);
        outb((TIMER_RATE >> 8) & 0xF, TIMER_PERIOD_IO_PORT);
        timer_ticks = 0;
}

/**
 * @brief Increment the timer tick value
 *        Will be invoked in the timer handler
 */
void kern_timer_tick() {
        timer_ticks++;
}

/**
 * @brief Get the current timer tick value
 * @return The current timer tick value
 */
unsigned int kern_timer_get_ticks() {
        return timer_ticks;
}
