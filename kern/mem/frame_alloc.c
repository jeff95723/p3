/**
 * @file frame_alloc.c
 * @brief The implementation for physical frame allocator
 * @author Hongyu Li (hongyul)
 * @bug No known bug
 */

#include "../inc/mem/frame_alloc.h"

#include <string.h>
#include <common_kern.h>
#include <syscall.h>
#include <string.h>
#include <stddef.h>
#include <malloc.h>

#include "../inc/mem/vm.h"
#include "../inc/task/task_mgr.h"
#include "../inc/util/asm_utils.h"
#include "../inc/util/logger.h"
#include "../inc/util/kmutex.h"


frame_alloc_t allocator;

/**
 * @brief The initializer of the physical frame allocator module
 */
void frame_alloc_init() {
        allocator.size = 0;
        allocator.capacity = machine_phys_frames() - KERNEL_PAGE_NUM;
        allocator.new_frame_addr = USER_MEM_START;
        allocator.free_frame = NULL;
        allocator.buffer = smemalign(PAGE_SIZE, PAGE_SIZE);
        kmutex_init(&allocator.mutex);
}

/**
 * @brief Allocates a new physical frame or retrieve one from the free frames
 *        for the caller
 * @return The start address of a free frame
 */
// TODO: add locking
void *new_frame() {
        // handle physical frame full
        kmutex_lock(&allocator.mutex);
        if (allocator.size >= allocator.capacity) {
                Log.e("Out of physical frames.");
                kmutex_unlock(&allocator.mutex);
                return NULL;
        }

        task_t *task = GET_CURRENT_TASK();
        void *new_frame;
        unsigned int new_entry;
        unsigned int old_entry;
        // if free frame exists, return free frame
        if (allocator.free_frame) {
                // init new physical page
                new_frame = allocator.free_frame;
                new_entry = (unsigned int) init_page_entry_flags(new_frame,
                                PAGE_READ_WRITE,
                                PAGE_USER_ACCESS);

                // direct map to new physical page
                old_entry = remap_page(task->dir_table,
                                allocator.buffer, new_entry);
                allocator.free_frame = *((void **) allocator.buffer);

                // Memset to 0
                memset(allocator.buffer, 0,PAGE_SIZE);

                // restore original mapping
                remap_page(task->dir_table, allocator.buffer, old_entry);
                allocator.size++;

                kmutex_unlock(&allocator.mutex);
                return new_frame;
        } else {
                // get a new physical page if no new frame exists
                new_frame = (void *) allocator.new_frame_addr;
                allocator.new_frame_addr += PAGE_SIZE;
                allocator.size++;

                kmutex_unlock(&allocator.mutex);
                return new_frame;
        }
}

/**
 * @brief Recycle the given physical frame and put it to free frames
 * @param free_phys_addr The start address of the physical frame to be recycled
 */
// TODO: add locking
void free_frame(void *free_phys_addr) {
        unsigned int phys_addr = FLUSH_VM_FLAGS(free_phys_addr);

        // free frame address should not be in kernel space
        if (phys_addr < USER_MEM_START) return;

        kmutex_lock(&allocator.mutex);
        unsigned int new_entry;
        unsigned int old_entry;
        task_t *task = GET_CURRENT_TASK();
        new_entry = (unsigned int) free_phys_addr;
        old_entry = remap_page(task->dir_table,
                        allocator.buffer, new_entry);
        *((void **) allocator.buffer) = allocator.free_frame;
        allocator.free_frame = free_phys_addr;
        allocator.size--;

        // restore original mapping
        remap_page(task->dir_table, allocator.buffer, old_entry);
        kmutex_unlock(&allocator.mutex);
}

int available_frames() {
        return (allocator.capacity - allocator.size);
}
