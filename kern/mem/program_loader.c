/**
 * @file program_loader.c
 * @brief Implementation for the program loader module
 * @author Jianfei Liu (jianfeil)
 * @bug No known bug
 */

#include "../inc/mem/program_loader.h"

#include <elf/elf_410.h>
#include <malloc_internal.h>
#include <stddef.h>
#include <string.h>
#include <syscall.h>
#include <simics.h>
#include <eflags.h>
#include <seg.h>
#include <cr.h>

#include "../inc/mem/vm.h"
#include "../inc/util/asm_utils.h"


#define PAGE_ALIGN(p) ((void *)((unsigned int) p & 0xFFFFF000));
#define GET_ALIGN_OFFSET(p) ((unsigned int) p & 0x00000FFF)
#define MIN(a, b) (a > b ? b : a)
#define PTR_ADD(p,v) ((void *)((unsigned int) p + v))
#define PTR_SUB(p,v) ((void *)((unsigned int) p - v))


void write_elf_data(void *dir_table,
                    void *vm_addr,
                    long offset,
                    long length,
                    const char* name,
                    int read_write_flag,
                    int user_access_flag);
void write_zero(void *dir_table,
                void* vm_addr,
                long length,
                int read_write_flag,
                int user_access_flag);
void* create_stack(void *dir_table);

task_t *load_program(const char *name) {
        void *stack_addr;
        void *entry_addr;
        void *kern_table = new_directory_table_with_id();
        task_t *task = create_task();
        thread_t *thread = task->thread;
        task->dir_table = kern_table;

        uint32_t page_directory = get_cr3();
        set_cr3((uint32_t) kern_table);
        if (load_elf(name, kern_table, &stack_addr, &entry_addr) < 0) {
                // TODO: restore state (free memory in task & thread)
                return NULL;
        }
        set_cr3(page_directory);

        uint32_t eflag = get_eflags();
        eflag |= EFL_RESV1;
        eflag |= EFL_IF;
        eflag &= (~EFL_IOPL_RING3);
        eflag &= (~EFL_AC);

        ctxt_switch_stack *stack = &(((thread_stack *) thread)->ctxt_stack);
        uint32_t ds = SEGSEL_USER_DS;
        stack->sreg.ds = ds;
        stack->sreg.es = ds;
        stack->sreg.fs = ds;
        stack->sreg.gs = ds;

        stack->iret.eip = (uint32_t) entry_addr;
        stack->iret.cs = SEGSEL_USER_CS;
        stack->iret.eflag = eflag;
        stack->iret.esp = (uint32_t) stack_addr;
        stack->iret.ss = SEGSEL_USER_DS;

        stack->rtn_addr = (unsigned long) iret_wrapper;
        thread->esp = (uint32_t) (&(stack->greg.ebp));

        return task;
}

int load_elf(const char *name, void *dir_table, void **stack_bottom_p,
        void **entry_p) {
        simple_elf_t elf;
        if (elf_check_header(name) < 0) return -1;
        if (elf_load_helper(&elf, name) < 0) return -1;

        // Need to load text rodata data bss
        // First we load text
        lprintf("Entry point %x", (unsigned int) elf.e_entry);
        write_elf_data(dir_table,
                       (void *) elf.e_txtstart,
                       elf.e_txtoff,
                       elf.e_txtlen,
                       name,
                       PAGE_READ_ONLY,
                       PAGE_USER_ACCESS);
        // Then we load data
        write_elf_data(dir_table,
                       (void *) elf.e_datstart,
                       elf.e_datoff,
                       elf.e_datlen,
                       name,
                       PAGE_READ_WRITE,
                       PAGE_USER_ACCESS);
        // Then we load rodata
        write_elf_data(dir_table,
                       (void *) elf.e_rodatstart,
                       elf.e_rodatoff,
                       elf.e_rodatlen,
                       name,
                       PAGE_READ_ONLY,
                       PAGE_USER_ACCESS);
        // Last we load bss
        write_zero(dir_table,
                   (void *) elf.e_bssstart,
                   elf.e_bsslen,
                   // This is used only without ZFOD
                   // TODO: ADD ZFOD!
                   PAGE_READ_WRITE,
//                   PAGE_READ_ONLY,
                   PAGE_USER_ACCESS);

        *stack_bottom_p = create_stack(dir_table);
        *entry_p = (void *)elf.e_entry;
        return 0;
}


void write_elf_data(void *dir_table,
                    void *vm_addr,
                    long offset,
                    long length,
                    const char* name,
                    int read_write_flag,
                    int user_access_flag) {
        long align_off = (long) GET_ALIGN_OFFSET(vm_addr);
        void *vm_addr_aligned = PAGE_ALIGN(vm_addr);
        char buf[PAGE_SIZE];
        while (length > 0) {

                int size = MIN(length, (PAGE_SIZE - align_off));
                getbytes(name, offset, size, buf);

                // Copy stuff to memory
                add_newpage(dir_table, vm_addr_aligned,
                                read_write_flag, user_access_flag);

                char *data_start = (char *)PTR_ADD(vm_addr_aligned, align_off);
                int i;
                // Shit breaks here
                for (i = 0; i < size; i++) {

                        // Need to convert data_start to vm
                        // Here assuming no paging.
                        data_start[i] = buf[i];
                }
                vm_addr_aligned = (void *) ((unsigned int)vm_addr_aligned +
                                PAGE_SIZE);
                offset = offset + size;
                length = length - size;
                align_off = 0;
        }
}

void write_zero(void *dir_table,
                void* vm_addr,
                long length,
                int read_write_flag,
                int user_access_flag) {
        long align_off = (long) GET_ALIGN_OFFSET(vm_addr);
        void *vm_addr_aligned = PAGE_ALIGN(vm_addr);
        while (length > 0) {
                int size = MIN(length, (PAGE_SIZE - align_off));
                // Copy stuff to memory
                add_newpage(dir_table, vm_addr_aligned,
                                read_write_flag, user_access_flag);
                void *data_start = (char *)PTR_ADD(vm_addr_aligned, align_off);
                memset(data_start, 0, size);

                vm_addr_aligned = (void *) ((unsigned int)vm_addr_aligned +
                                PAGE_SIZE);
                length = length - size;
                align_off = 0;
        }

}

/**
 * @param dir_table The users directory table
 * @return bottom of the new stack.
 */
void* create_stack(void *dir_table) {
    long stack_size  = (STACK_SIZE + 1) * PAGE_SIZE; // One more page for pars
    write_zero(dir_table,
               (void *)(STACK_START - stack_size),
               stack_size,
               PAGE_READ_WRITE,
               PAGE_USER_ACCESS);
    return (void *)(STACK_START - PAGE_SIZE);
}

