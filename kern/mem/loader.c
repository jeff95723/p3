/**
 * The 15-410 kernel project.
 * @name loader.c
 *
 * Functions for the loading
 * of user programs from binary
 * files should be written in
 * this file. The function
 * elf_load_helper() is provided
 * for your use.
 */
/*@{*/

/* --- Includes --- */
#include <../inc/mem/loader.h>

#include <string.h>
#include <stdio.h>
#include <malloc.h>
#include <exec2obj.h>
#include <elf_410.h>
#include <simics.h>

/* --- Local function prototypes --- */

/**
 * Copies data from a file into a buffer.
 *
 * @param filename   the name of the file to copy data from
 * @param offset     the location in the file to begin copying from
 * @param size       the number of bytes to be copied
 * @param buf        the buffer to copy the data into
 *
 * @return returns the number of bytes copied on succes; -1 on failure
 */
int getbytes(const char *filename, int offset, int size, char *buf) {

        /*
         * You fill in this function.
         */
        int entry_index = 0;
        int copy_size = 0;
        if (filename == NULL || buf == NULL) {
                return -1;
        }
        while (entry_index < exec2obj_userapp_count) {
                if (strcmp(exec2obj_userapp_TOC[entry_index].execname,
                                filename) == 0) {
                        while (copy_size < size &&
                               copy_size + offset <
                               exec2obj_userapp_TOC[entry_index].execlen) {
                                buf[copy_size] =
                                        exec2obj_userapp_TOC[entry_index].
                                        execbytes[copy_size + offset];
                                copy_size++;
                        }
                        return copy_size;
                }
                entry_index++;
        }

        return -1;
}

/*@}*/
