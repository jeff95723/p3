/**
 * @file vm.c
 * @brief Implementation for the virtual memory module
 * @author Jianfei Liu (jianfeil)
 *         Hongyu Li (hongyul)
 * @bug No known bug
 */


#include "../inc/mem/vm.h"

#include <malloc.h>
#include <stddef.h>
#include <syscall.h>
#include <string.h>
#include <cr.h>
#include <simics.h>

#include "../inc/mem/frame_alloc.h"
#include "../inc/task/task_mgr.h"
#include "../inc/util/asm_utils.h"
#include "../inc/util/logger.h"

/*
 * FOR ZFOD Have one all zero page and let all pages that are blank point to it
 * logically read write, physically read only, in page fault handler check
 * the fault address, if special blank page, do allocation and write to the
 * new page.
 */

static void *identity_directory_table;

void* new_directory_table();
void init_identity_paging(void * dir_table, int size);
void* init_directory_entry_flags(void *dir_entry) ;
void* init_page_entry_flags(void *page_entry,
                int read_write_flag, int user_access_flag);
void identity_paging(uint32_t *page_table, uint32_t pt_index);
void copy_page_table(unsigned int *buf_pt_entry_addr, void *buf_addr,
                void *src_page_table, void *dst_page_table, int dt_index);
void free_page_table(void *dir_table);

void init_paging() {
        identity_directory_table = new_directory_table();
        init_identity_paging(identity_directory_table,
                        IDENTITY_MAPPING_SIZE);
        return;
}

void enable_paging() {
        uint32_t cr0 = get_cr0();
        cr0 = cr0 | CR0_PG;
        set_cr0(cr0);
}

void disable_paging() {
        uint32_t cr0 = get_cr0();
        cr0 = cr0 & ~(CR0_PG);
        set_cr0(cr0);
}

void* new_directory_table() {
        // We need to page align this addr.
        void* dir_table = smemalign(PAGE_SIZE,
                (DIR_TABLE_SIZE * sizeof(int)));
        memset(dir_table, 0, DIR_TABLE_SIZE * sizeof(int));
        return dir_table;
}

void* new_page_table() {
        // We need to page align this addr.
        void* page_table = smemalign(PAGE_SIZE,
                (PAGE_TABLE_SIZE * sizeof(int)));
        memset(page_table, 0, PAGE_TABLE_SIZE * sizeof(int));
        return page_table;
}

void *new_directory_table_with_id() {
        void *new_table = new_directory_table();
        int pt_count = IDENTITY_MAPPING_SIZE / (PAGE_TABLE_SIZE * PAGE_SIZE);
        int *dt = (int *) new_table;
        int *id_dt = (int *) identity_directory_table;
        int i;
        for (i = 0; i < pt_count; i++) {
                dt[i] = id_dt[i];
        }

        return new_table;
}

void *get_identity_paging_table() {
        return identity_directory_table;
}

void init_identity_paging(void * dir_table, int size) {
        int pt_count = size / (PAGE_TABLE_SIZE * PAGE_SIZE);
        lprintf("pt_count: %d", pt_count);
        int* dt  = (int *) dir_table;
        void* entry;
        int i;

        for (i = 0; i < pt_count; i++) {
                entry = new_page_table();
                identity_paging((uint32_t *) entry, (uint32_t) i);
                SET_PRESENT(entry);
                SET_READ_WRITE(entry);
                dt[i] = (int) entry;
        }
}

void identity_paging(uint32_t *page_table, uint32_t pt_index) {
        int i;
        void *entry;

        for (i = 0; i < PAGE_TABLE_SIZE; i++) {
                entry = (void *) (pt_index << 22);
                entry = (void *)((unsigned int) entry |
                                (unsigned int) (i << 12));
                SET_PRESENT(entry);
                SET_READ_WRITE(entry);
                page_table[i] = (uint32_t) entry;
        }
}

void copy_dir_table(void *src_dir_table, void *dst_dir_table, void *buf_addr) {
        if (!(src_dir_table) || !(dst_dir_table)) return;
        unsigned int *src_dt = (unsigned int *) src_dir_table;
        unsigned int *dst_dt = (unsigned int *) dst_dir_table;

        int dt_index = IDENTITY_MAPPING_SIZE / (PAGE_TABLE_SIZE * PAGE_SIZE);

        // get the address of the two pages in current thread stack
        unsigned int buf = ((unsigned int) buf_addr);

        // transform them into corresponding directory table and page table index
        unsigned int buf_dir_index = GET_DIR_INDEX(buf);
        unsigned int buf_pg_index  = GET_PAGE_INDEX(buf);

        // get the original directory table entry
        unsigned int buf_dir_entry=
                        ((unsigned int *)src_dir_table)[buf_dir_index];

        // get the original page table entry
        unsigned int *buf_page_table =
                        ((unsigned int *) FLUSH_VM_FLAGS(buf_dir_entry));
        unsigned int buf_pg_entry = buf_page_table[buf_pg_index];

        for (; dt_index < DIR_TABLE_SIZE; dt_index ++) {
                unsigned int entry = src_dt[dt_index];
                // Got a present dir entry
                if (GET_PRESENT(entry)) {
                        unsigned int flags = GET_VM_FLAGS(entry);
                        void *new_pt = new_page_table();
                        copy_page_table(&(buf_page_table[buf_pg_index]),
                                        buf_addr,
                                        (void *)FLUSH_VM_FLAGS(entry),
                                        new_pt,
                                        dt_index);
                        // Use the old flags for the new entry
                        dst_dt[dt_index] = ((FLUSH_VM_FLAGS(new_pt)) | flags);
                }
        }
        // restore the original paging
        buf_page_table[buf_pg_index] = buf_pg_entry;
        invalidate_vm_page(buf_addr);
}

void free_dir_table(void *dir_table) {
        int dt_index = IDENTITY_MAPPING_SIZE / (PAGE_TABLE_SIZE * PAGE_SIZE);

        unsigned int *src_dt = (unsigned int *) dir_table;

        for (; dt_index < DIR_TABLE_SIZE; dt_index ++) {
                unsigned int entry = src_dt[dt_index];
                // Got a present dir entry
                if (GET_PRESENT(entry)) {
                        free_page_table((void *) FLUSH_VM_FLAGS(entry));
                        sfree((void *) FLUSH_VM_FLAGS(entry), PAGE_SIZE);
                }
        }
        sfree((void *) FLUSH_VM_FLAGS(dir_table), PAGE_SIZE);

        return;
}

void free_page_table(void *page_table) {
        int i;
        unsigned int *src_pt = (unsigned int *) page_table;
        for (i = 0; i < PAGE_TABLE_SIZE; i++) {
                unsigned int entry = src_pt[i];
                //Log.e("Page entry to free: %p", (void *) entry);
                if (GET_PRESENT(entry)) {
                        //Log.e("Page entry to free: %p", (void *) entry);
                        // Call frame recycle
                        free_frame((void *) entry);
                }
        }
}

void copy_page_table(unsigned int *buf_pt_entry_addr, void *buf_addr,
                void *src_page_table, void *dst_page_table, int dt_index) {
        if (!(src_page_table) || !(dst_page_table)) return;
        unsigned int *src_pt = (unsigned int *) src_page_table;
        unsigned int *dst_pt = (unsigned int *) dst_page_table;
        int i;

        for (i = 0; i < PAGE_TABLE_SIZE; i++) {
                unsigned int entry = src_pt[i];
                if (GET_PRESENT(entry)) {
                        unsigned int dst_entry = (unsigned int) new_frame();
                        unsigned int flags = GET_VM_FLAGS(entry);
                        dst_entry = (FLUSH_VM_FLAGS(dst_entry) | flags);

                        // remap dest entry to the stack buffer
                        *buf_pt_entry_addr= dst_entry;

                        // Invalidate the cached remapped page.
                        invalidate_vm_page(buf_addr);

                        // write data from src to dest
                        char *from_vm = (char *) ((dt_index << 22) | (i << 12));
                        char *to_vm = (char *) buf_addr;

                        int j;
                        for (j = 0; j < PAGE_SIZE; j++) {
                                to_vm[j] = from_vm[j];
                        }

                        // put dst entry to page table
                        dst_pt[i] = dst_entry;
                }
        }
}

void* add_newpage(void *dst_dir_table,
                  void *src_vm_addr,
                  int read_write_flag,
                  int user_access_flag) {
        int dir_index = GET_DIR_INDEX(src_vm_addr);
        int page_index = GET_PAGE_INDEX(src_vm_addr);
        unsigned int *directory_table = (unsigned int *) dst_dir_table;
        unsigned int *page_table;

        unsigned int directory_entry = directory_table[dir_index];

        if (GET_PRESENT(directory_entry) == 0) {
                page_table = (unsigned int *)new_page_table();
                directory_table[dir_index] = (unsigned int)init_directory_entry_flags(page_table);
        } else {
                page_table = (unsigned int *)FLUSH_VM_FLAGS(directory_entry);
        }

        void *page = (void *) page_table[page_index];
        if (GET_PRESENT(page) == 0) {
                void *page_entry = new_frame();
                page_table[page_index] = (unsigned int) init_page_entry_flags(page_entry, read_write_flag, user_access_flag);
                return page_entry;
        } else {
                return (void *)FLUSH_VM_FLAGS(page);
        }
}

void remove_newpage(void *dst_dir_table,
                  void *src_vm_addr) {
        int dir_index = GET_DIR_INDEX(src_vm_addr);
        int page_index = GET_PAGE_INDEX(src_vm_addr);
        unsigned int *directory_table = (unsigned int *) dst_dir_table;
        unsigned int *page_table;

        unsigned int directory_entry = directory_table[dir_index];

        if (GET_PRESENT(directory_entry) == 0) {
                return;
        } else {
                page_table = (unsigned int *)FLUSH_VM_FLAGS(directory_entry);
                Log.d("remove_newpage: found page table: %p.", page_table);
        }

        void *page = (void *) page_table[page_index];
        if (GET_PRESENT(page) == 0) {
                return;
        } else {
                Log.d("remove_newpage: found page: %p.", page);
                Log.d("remove_newpage: freeing frame: %p.", (void *) FLUSH_VM_FLAGS(page));
                free_frame(page);
                page_table[page_index] = 0;
                invalidate_vm_page(src_vm_addr);
                return;
        }
}

unsigned int remap_page(void *src_dir_table,
                        void *buf_addr,
                        unsigned int target_entry) {
        // get the address of the two pages in current thread stack
        unsigned int buf = ((unsigned int) buf_addr);

        // transform them into corresponding directory table and page table index
        unsigned int buf_dir_index = GET_DIR_INDEX(buf);
        unsigned int buf_pg_index  = GET_PAGE_INDEX(buf);

        // get the original directory table entry
        unsigned int buf_dir_entry =
                        ((unsigned int *) src_dir_table)[buf_dir_index];

        // get the original page table entry
        unsigned int *buf_page_table =
                        ((unsigned int *) FLUSH_VM_FLAGS(buf_dir_entry));
        unsigned int buf_pg_entry = buf_page_table[buf_pg_index];

        // remap page entry
        buf_page_table[buf_pg_index] = target_entry;
        invalidate_vm_page(buf_addr);
        return buf_pg_entry;
}

int virtual_to_physical_address(void *dir_table, void *vm, void ** p) {
        int dir_index = GET_DIR_INDEX(vm);
        int page_index = GET_PAGE_INDEX(vm);
        unsigned int *dt = (unsigned int *) dir_table;
        unsigned int *pt;

        unsigned int dir_entry = dt[dir_index];

        // Dir entry is not present, error
        if (GET_PRESENT(dir_entry) == 0) {
                // Dir entry not present
                return -1;
        }

        pt = (unsigned int *)FLUSH_VM_FLAGS(dir_entry);

        void *page = (void *) pt[page_index];
        if (GET_PRESENT(page) == 0) {
                // Page table entry is not present
                return -1;
        }

        *p = page;
        return 0;
}

void* init_directory_entry_flags(void *dir_entry) {
        SET_PRESENT(dir_entry);
        UNSET_ACCESSED(dir_entry);
        UNSET_DISABLE_CACHE(dir_entry);
        UNSET_WRITE_TRU(dir_entry);
        UNSET_DIRTY(dir_entry);
        UNSET_PAGE_SIZE_FLAG(dir_entry);

        // Demo only
        SET_READ_WRITE(dir_entry);
        SET_USER_ACCESS(dir_entry);

        return dir_entry;
}

void* init_page_entry_flags(void *page_entry,
                            int read_write_flag,
                            int user_access_flag) {
        SET_PRESENT(page_entry);
        UNSET_ACCESSED(page_entry);
        UNSET_DISABLE_CACHE(page_entry);
        UNSET_WRITE_TRU(page_entry);
        UNSET_DIRTY(page_entry);
        SET_PAGE_SIZE_FLAG(page_entry);

        // Demo only
        if (read_write_flag) {
                SET_READ_WRITE(page_entry);
        } else {
                UNSET_READ_WRITE(page_entry);
        }

        if (user_access_flag) {
                SET_USER_ACCESS(page_entry);
        } else {
                UNSET_USER_ACCESS(page_entry);
        }

        return page_entry;
}
